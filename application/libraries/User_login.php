<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_Login
{
    
     public function __construct()
     {
         $this->ci=&get_instance();
         $this->ci->load->model('Auth_model');
     }
    public function login($username,$password)
    {
        $cek = $this->ci->Auth_model->login_user($username,$password);
        if($cek){
            $id = $cek->id;
            $nama = $cek->nama;
            $username = $cek->username;
            $password = $cek->password;
            $level = $cek->level;
            $phone = $cek->phone;
            $foto = $cek->foto;

            $this->ci->session->set_userdata('id', $id);
            $this->ci->session->set_userdata('username', $username);
            $this->ci->session->set_userdata('password', $password); 
            $this->ci->session->set_userdata('nama', $nama);
            $this->ci->session->set_userdata('level', $level);           
            $this->ci->session->set_userdata('phone', $phone);
            $this->ci->session->set_userdata('foto', $foto);            

            $this->ci->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Login Berhasil!
	  </div>');  
            redirect('dashboard');  
        }else{
            $this->ci->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Login Gagal! Username atau Password salah
	  </div>');  
            redirect('auth');                                               
        }
    }

    function user_login(){
        $this->ci->load->model('Auth_model');
        $username=$this->ci->session->userdata('username');
        $password=$this->ci->session->userdata('password');
        $user_data = $this->ci->Auth_model->login_user($username,$password);
        return $user_data;
        }

    public function proteksi_halaman()
    {
        if($this->ci->session->userdata('username')==''){
            $this->ci->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Anda Belum Login!
	  </div>');
            redirect('auth');
            
        }
    }    

    public function logout()
    {
        $this->ci->session->unset_userdata('username');
        $this->ci->session->unset_userdata('nama');
        $this->ci->session->unset_userdata('level');
            redirect('auth/login_user');
            
        }

}
