<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('Kabupaten_model');
	}

	public function get_all()
	{                
        $this->db->select('a.*,b.nama_prop');
        $this->db->from('kabupaten a');
        $this->db->join('propinsi b', 'a.id_propinsi=b.id_propinsi');
        $this->db->order_by('a.nama_kab', 'asc');
        return $this->db->get()->result();
    }

    public function get_kab($keyword)
	{
          $this->db->select('*');
          $this->db->from('kabupaten');
          $this->db->like('nama_kab', $keyword);
          $this->db->order_by('nama_kab', 'asc');
          return $this->db->get()->result();
    }

    public function get_count()
	{
        $sql = "SELECT count(id_kabupaten) as id_kabupaten from kabupaten";
        $result = $this->db->query($sql);
        return $result->row()->id_kabupaten; 
                
        }
    
	public function insert($data)
	{
        $this->db->insert('kabupaten', $data);
                
        }
           
   public function update($id_kabupaten, $data)
	{
        $this->db->where('id_kabupaten', $id_kabupaten);
        $this->db->update('kabupaten', $data);
        
        }

    public function get_by_id($id_kabupaten)
	{
         $this->db->where('id_kabupaten', $id_kabupaten);
         return $this->db->get('kabupaten')->row();
                
        } 

    public function delete($id_kabupaten)
	{
        $this->db->where('id_kabupaten', $id_kabupaten);
        $this->db->delete('kabupaten');
                        
        }
          
}
