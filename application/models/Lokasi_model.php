<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi_model extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Lokasi_model');
	}
	
	public function get_by_id($id_lokasi)
	{
         $this->db->where('id_lokasi', $id_lokasi);
         return $this->db->get('lokasi')->row();
                
    }    

    public function count_view1()
        {
            $sql = "SELECT count(nama_instansi) as nama_instansi from view_1";
            $result = $this->db->query($sql);
            return $result->row()->nama_instansi; 
                    
            }
        
        public function get_count()
        {
            $sql = "SELECT count(id_lokasi) as id_lokasi from lokasi";
            $result = $this->db->query($sql);
            return $result->row()->id_lokasi; 
                    
            }            

        public function get_view($keyword)
        {   
            $this->db->select('a.*,b.nama_kab,c.nama_prop,d.jenis, COUNT(e.id_mhs) as jml_mhs');
            $this->db->from('lokasi a');
            $this->db->join('kabupaten b', 'a.id_kabupaten=b.id_kabupaten');
            $this->db->join('propinsi c', 'a.id_propinsi=c.id_propinsi');
            $this->db->join('jenis_lokasi d', 'a.id_jenis=d.id_jenis');
            $this->db->join('mahasiswa e', 'a.id_lokasi=e.id_lokasi');
            $this->db->group_by('a.nama_instansi');
            $this->db->like('a.nama_instansi', $keyword);
            $this->db->order_by('jml_mhs', 'desc');
            return $this->db->get()->result();
         }

         public function get_view1()
	{
        $this->db->select('a.*,b.nama_kab,c.nama_prop,d.jenis, COUNT(e.id_mhs) as jml_mhs');
            $this->db->from('lokasi a');
            $this->db->join('kabupaten b', 'a.id_kabupaten=b.id_kabupaten');
            $this->db->join('propinsi c', 'a.id_propinsi=c.id_propinsi');
            $this->db->join('jenis_lokasi d', 'a.id_jenis=d.id_jenis');
            $this->db->join('mahasiswa e', 'a.id_lokasi=e.id_lokasi');
            $this->db->group_by('a.nama_instansi');
            $this->db->order_by('jml_mhs', 'desc');
            return $this->db->get()->result(); 
    }
         
         public function get_top_10()
        {
            $this->db->select('a.*,b.nama_kab,c.nama_prop,d.jenis, COUNT(e.id_mhs) as jml_mhs');
            $this->db->from('lokasi a');
            $this->db->join('kabupaten b', 'a.id_kabupaten=b.id_kabupaten');
            $this->db->join('propinsi c', 'a.id_propinsi=c.id_propinsi');
            $this->db->join('jenis_lokasi d', 'a.id_jenis=d.id_jenis');
            $this->db->join('mahasiswa e', 'a.id_lokasi=e.id_lokasi');
            $this->db->group_by('a.nama_instansi');
            $this->db->order_by('jml_mhs', 'desc');
            $this->db->limit(10);
            return $this->db->get()->result();
         }
    
    public function get_all()
	{
        $this->db->select('a.*,b.nama_kab,c.nama_prop,d.jenis,d.marker_jenis');
        $this->db->from('lokasi a');
        $this->db->join('kabupaten b', 'a.id_kabupaten=b.id_kabupaten');
        $this->db->join('propinsi c', 'a.id_propinsi=c.id_propinsi');
        $this->db->join('jenis_lokasi d', 'a.id_jenis=d.id_jenis');
        $this->db->order_by('a.nama_instansi', 'asc');
        return $this->db->get()->result();  
    }
   

    public function get_lokasi($id_kabupaten)
	{
        $this->db->select('a.*,b.nama_kab,c.nama_prop,d.jenis');
        $this->db->from('lokasi a');
        $this->db->join('kabupaten b', 'a.id_kabupaten=b.id_kabupaten');
        $this->db->join('propinsi c', 'a.id_propinsi=c.id_propinsi');
        $this->db->join('jenis_lokasi d', 'a.id_jenis=d.id_jenis');
        $this->db->like('b.id_kabupaten', $id_kabupaten);
        $this->db->order_by('a.id_lokasi', 'asc');
        return $this->db->get()->result();  
    }

    public function get_cari($keyword)
	{
          $this->db->select('*');
          $this->db->from('view_1');
          $this->db->like('nama_instansi', $keyword);
          return $this->db->get()->result();
    }

    public function insert($data)
	{
        $this->db->insert('lokasi', $data);
                
        }
           
   public function update($id_lokasi, $data)
	{
        $this->db->where('id_lokasi', $id_lokasi);
        $this->db->update('lokasi', $data);
        
        }

        public function update_mitra($data, $where)
        {
            $this->db->where($where);
            $this->db->update('lokasi', $data);
            return TRUE;
            }   

    public function delete($id_lokasi)
	{
        $this->db->where('id_lokasi', $id_lokasi);
        $this->db->delete('lokasi');
                        
        }
}
