<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_lokasi_model extends CI_Model {

    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_lokasi_model');
	}
	
	public function get_by_id($id_jenis)
	{
         $this->db->where('id_jenis', $id_jenis);
         return $this->db->get('jenis_lokasi')->row();
                
        }       
    
    public function get_all()
	{
        $this->db->order_by('id_jenis', 'asc');
        return $this->db->get('jenis_lokasi')->result();  
    }

    public function insert($data)
	{
        $this->db->insert('jenis_lokasi', $data);
                
        }
           
   public function update($id_jenis, $data)
	{
        $this->db->where('id_jenis', $id_jenis);
        $this->db->update('jenis_lokasi', $data);
        
        }

    public function delete($id_jenis)
	{
        $this->db->where('id_jenis', $id_jenis);
        $this->db->delete('jenis_lokasi');
                        
        }

        public function upload($data, $where)
        {
            $this->db->where($where);
            $this->db->update('jenis_lokasi', $data);
            return TRUE;
            } 
}
