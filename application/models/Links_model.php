<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Links_model extends CI_Model {

    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Links_model');
	}
	
	public function get_by_id($id_link)
	{
         $this->db->where('id_link', $id_link);
         return $this->db->get('links')->row();
                
        }       
    
    public function get_all()
	{
        $this->db->order_by('id_link', 'asc');
        return $this->db->get('links')->result();  
    }
    
    public function insert($data)
	{
        $this->db->insert('links', $data);
                
        }
           
   public function update($id_link, $data)
	{
        $this->db->where('id_link', $id_link);
        $this->db->update('links', $data);
        
        }

    public function delete($id_link)
	{
        $this->db->where('id_link', $id_link);
        $this->db->delete('links');
                        
        }
}
