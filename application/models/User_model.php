<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function ambillogin($username,$password){
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('user');
        if ($query->num_rows()>0){
            foreach ($query->result() as $row){
                $sess = array ('username' => $row->username, 'password' => $row->password);
            }
        $this->session->get_userdata($sess);

        redirect('dashboard');   
        }else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Username atau password yang anda masukkan salah! Mohon masukkan username dan password dengan benar. </div>');
            redirect('login');
        }
    }

    public function login($post)    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('username', $post['username']);
        $this->db->where('password', $post['password']);
        $this->db->where('nama', $post['nama']);
        $query = $this->db->get();
        return $query;
    }

    public function get($id = null)
    {
	$this->db->from('user');
	if($id !=null){
		$this->db->where('user_id', $id);
	}
	$query = $this->db->get();
	return $query;
    }

    public function update($id, $data)
	{
        $this->db->where('id', $id);
        $this->db->update('user', $data);
        
        }
        public function update_profile($data, $where)
        {
            $this->db->where($where);
            $this->db->update('user', $data);
            return TRUE;
            }        
    
    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('user')->row();
                    
        }
    public function get_all()
    {
        $this->db->order_by('id', 'asc');
        return $this->db->get('user')->result();  
        }

    public function insert($data)
	{
        $this->db->insert('user', $data);
                
        }

    public function delete($id)
	{
        $this->db->where('id', $id);
        $this->db->delete('user');
                        
        }                
}
