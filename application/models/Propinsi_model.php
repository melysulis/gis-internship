<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propinsi_model extends CI_Model {

    
    public function __construct()
	{
		parent::__construct();
		$this->load->model('Propinsi_model');
	}
	
	public function get_by_id($id_propinsi)
	{
         $this->db->where('id_propinsi', $id_propinsi);
         return $this->db->get('propinsi')->row();
                
        }       
    
    public function get_all()
	{
        $this->db->select('*');
        $this->db->from('propinsi');
        $this->db->order_by('nama_prop', 'asc');
        return $this->db->get()->result();  
    }

    public function get_count()
	{
        $sql = "SELECT count(id_propinsi) as id_propinsi from propinsi";
        $result = $this->db->query($sql);
        return $result->row()->id_propinsi; 
                
        }

    public function insert($data)
	{
        $this->db->insert('propinsi', $data);
                
        }
           
   public function update($id_propinsi, $data)
	{
        $this->db->where('id_propinsi', $id_propinsi);
        $this->db->update('propinsi', $data);
        
        }

    public function delete($id_propinsi)
	{
        $this->db->where('id_propinsi', $id_propinsi);
        $this->db->delete('propinsi');
                        
        }
}
