<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Options_model extends CI_Model {

    
	public function get_by_id($id_options)
	{
         $this->db->where('id_options', $id_options);
         return $this->db->get('options')->row();
                
        }       
    
    public function get_all()
	{
        $this->db->order_by('id_options', 'asc');
        return $this->db->get('options')->result();  
    }

    function update($id_options, $data)
	{
		$this->db->where('id_options', $id_options);
		$this->db->update('options', $data);
		$this->db->where('id_options', $id_options);
		return $this->db->get('options')->row();
	}
}
