<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_model');
	}

    public function count(){
        
    }

	public function get_all()
	{                
        $this->db->select('a.*,b.nama_instansi');
        $this->db->from('mahasiswa a');
        $this->db->join('lokasi b', 'a.id_lokasi=b.id_lokasi');
        $this->db->order_by('a.nim', 'asc');
        return $this->db->get()->result();
    }

    public function get_count()
	{
        $sql = "SELECT count(id_mhs) as id_mhs from mahasiswa";
        $result = $this->db->query($sql);
        return $result->row()->id_mhs; 
                
    }
    
    public function get_mhs($keyword)
        {   
            $this->db->select('a.*,b.nama_instansi');
            $this->db->from('mahasiswa a');
            $this->db->join('lokasi b', 'a.id_lokasi=b.id_lokasi');
          $this->db->like('nama', $keyword);
          $this->db->order_by('a.nim', 'asc');
          return $this->db->get()->result();  
         }
                
    
	public function insert($data)
	{
        $this->db->insert('mahasiswa', $data);
                
        }
           
   public function update($id_mhs, $data)
	{
        $this->db->where('id_mhs', $id_mhs);
        $this->db->update('mahasiswa', $data);
        
        }

    public function get_by_id($id_mhs)
	{
         $this->db->where('id_mhs', $id_mhs);
         return $this->db->get('mahasiswa')->row();
                
        } 

    public function delete($id_mhs)
	{
        $this->db->where('id_mhs', $id_mhs);
        $this->db->delete('mahasiswa');
                        
        }
          
}
