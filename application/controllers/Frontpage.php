<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('googlemaps');		
		$this->load->model('Lokasi_model');
		$this->load->model('Jenis_lokasi_model');
		$this->load->model('Mahasiswa_model');
		$this->load->model('Kabupaten_model');
		$this->load->model('Options_model');
		$this->load->model('Links_model');
	}

	public function beranda(){
		$keyword=$this->input->post('keyword');
		$kab=$this->Kabupaten_model->get_kab($keyword);
		foreach ($kab as $key => $row) {
			if(!$keyword ){
				$config['center'] = '-7.3325227161676985, 109.41400139320258';
			}		else{
				$config['center'] = "$row->latitude,$row->longitude";
			}
		}
		$config['zoom'] = 11;
		$this->googlemaps->initialize($config);				
		
		$lokasi=$this->Lokasi_model->get_all();
			foreach ($lokasi as $key => $value) {
			$marker=array();
			$marker['position']="$value->latitude,$value->longitude";
			$marker['animation']="DROP";
			$marker['infowindow_content'] = '<div class="media" style="width:250px;">';
			$marker['infowindow_content'] .= '<div class="media-body">';
			$marker['infowindow_content'] .= '<h5> '.$value->nama_instansi.'</h5>';
			$marker['infowindow_content'] .= '<img src=" '.base_url('uploads/' .$value->icon).'" width="50%">';
			$marker['infowindow_content'] .= '<br> </br>';
			$marker['infowindow_content'] .= '<p>Alamat : '.$value->alamat.'</p>';
			$marker['infowindow_content'] .= '<p>No Tlp : '.$value->no_tlp.'</p>';
			$marker['infowindow_content'] .= '<p>Email : '.$value->email.'</p>';
			$marker['infowindow_content'] .= '<a href=" '.$value->website.' "> <p>Website : '.$value->website.'</p>';
			$marker['infowindow_content'] .= '</div>';
			$marker['infowindow_content'] .= '</div>';
			$this->googlemaps->add_marker($marker);
		}
		
		$data=array(			
			'data_mitra'=>$this->Lokasi_model->get_all(),
			'data_lokasi'=>$this->Lokasi_model->get_all(),
			'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
			'kab'=>$this->Kabupaten_model->get_kab($keyword),
			'data_kab'=>$this->Kabupaten_model->get_all(),
			/*'nama_kab'=>set_value('nama_kab', $keyword),*/
			'data_top10'=>$this->Lokasi_model->get_top_10(),
			'data_link'=>$this->Links_model->get_all(),
			'link_surat'=>$this->Links_model->get_by_id(3),
			'tentang' => $this->Options_model->get_by_id(1),
			'title' => $this->Options_model->get_by_id(2),
			'nama_web' => $this->Options_model->get_by_id(7),
			'caption' => $this->Options_model->get_by_id(5),
			'footer' => $this->Options_model->get_by_id(6),
			'map'=> $this->googlemaps->create_map(),
			'count_mhs'=>$this->Mahasiswa_model->get_count(),
			'count_lokasi'=>$this->Lokasi_model->get_count()
		);
		$this->Kabupaten_model->get_kab($this->input->post('keyword'), $data);
		$this->load->view('front/beranda', $data);
	}
	
	public function detail_mhs()		
	{
		$keyword=$this->input->post('keyword');
		$data=array(
			'nama_web' => $this->Options_model->get_by_id(7),
		'data_mhs'=>$this->Mahasiswa_model->get_mhs($keyword),
		'footer' => $this->Options_model->get_by_id(6),
		'data_link'=>$this->Links_model->get_all()
		);
        $this->template->load('front/detail', 'front/mahasiswa', $data);
	
	}

	public function detail_lokasi()		
	{		
		$keyword=$this->input->post('keyword');
		$data=array(		
			'nama_web' => $this->Options_model->get_by_id(7),
			'footer' => $this->Options_model->get_by_id(6),
			'view_1'=>$this->Lokasi_model->get_view($keyword),
			'data_kab'=>$this->Kabupaten_model->get_all(),
			'view_all'=>$this->Lokasi_model->get_view1(),
		'data_link'=>$this->Links_model->get_all(),
		'data_lokasi'=>$this->Lokasi_model->get_all(),
		'count_lokasi'=>$this->Lokasi_model->get_count());
		$this->Lokasi_model->get_view($keyword, $data);
        $this->template->load('front/detail', 'front/lokasi', $data);
	
	}

	
	public function index()
	{	
		$data=array(			
			'data_mitra'=>$this->Lokasi_model->get_all(),
			'data_lokasi'=>$this->Lokasi_model->get_all(),
			'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
			'data_top10'=>$this->Lokasi_model->get_top_10(),
			'data_link'=>$this->Links_model->get_all(),
			'link_surat'=>$this->Links_model->get_by_id(3),
			'tentang' => $this->Options_model->get_by_id(1),
			'title' => $this->Options_model->get_by_id(2),
			'nama_web' => $this->Options_model->get_by_id(7),
			'caption' => $this->Options_model->get_by_id(5),
			'footer' => $this->Options_model->get_by_id(6),
			'map'=> $this->googlemaps->create_map(),
			'count_mhs'=>$this->Mahasiswa_model->get_count(),
			'count_lokasi'=>$this->Lokasi_model->get_count()
		);
		$this->load->view('front/beranda', $data);
	}
	
}
