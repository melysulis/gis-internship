<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mitra extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kabupaten_model');
		$this->load->model('Propinsi_model');
		$this->load->model('Lokasi_model');
		$this->load->model('User_model');
		$this->load->model('Jenis_lokasi_model');
		$this->load->library('googlemaps');
	} 
	
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_mitra'=>$this->Lokasi_model->get_all(),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto));
        $this->template->load('layout/master', 'mitra/mitra_list', $data);
		}
	
	}
	
	public function update(){
		$this->load->library('upload');
		$path ='./uploads/';
		$config['upload_path']=$path;
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='2048';
		$config['max_width']='1024';
		$config['max_height']='768';
		$nama_file="icon_".time();
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);

		$id_lokasi=$this->input->post('id_lokasi');
		$gambar_lama=$this->input->post('ganti_gambar');
		if($_FILES['icon']['name'])
		{
			$field_name="icon";
			if($this->upload->do_upload($field_name))
			{
				$id_lokasi=$this->input->post('id_lokasi');
			$id_kabupaten=$this->input->post('id_kabupaten');
			$id_propinsi=$this->input->post('id_propinsi');
			$nama_instansi=$this->input->post('nama_instansi');
			$id_jenis=$this->input->post('id_jenis');
			$alamat=$this->input->post('alamat');
			$no_tlp=$this->input->post('no_tlp');
			$email=$this->input->post('email');
			$website=$this->input->post('website');
			$latitude=$this->input->post('latitude');
			$longitude=$this->input->post('longitude');
			$icon=$this->upload->data();
			$lokasi=([
				'id_lokasi' => $id_lokasi,
				'id_kabupaten' => $id_kabupaten,
				'id_propinsi' => $id_propinsi,
				'nama_instansi' => $nama_instansi,
				'id_jenis' => $id_jenis,
				'alamat' => $alamat,
				'no_tlp' => $no_tlp,
				'email' => $email,
				'website' => $website,
				'latitude' => $latitude,
				'longitude' => $longitude,
				'icon' => $icon['file_name']			
			]);
			$data = array_merge($lokasi);
			@unlink($path.$gambar_lama);
			$where = array('id_lokasi'=>$id_lokasi);
			if($this->Lokasi_model->update_mitra($data, $where) == true)
			{
				$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Icon Berhasil Diubah!
		  </div>');
			redirect(site_url('mitra'));
			}
			else
			{
				$this->index();
			}
			}
		}
		else
		{
			$this->index();
		}
	}
	
}
