<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kabupaten_model');
		$this->load->model('Propinsi_model');
		$this->load->model('Lokasi_model');
		$this->load->model('User_model');
		$this->load->model('Jenis_lokasi_model');
		$this->load->library('googlemaps');
	} 
	
	public function index()
	{				
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_lokasi'=>$this->Lokasi_model->get_all(),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto));
        $this->template->load('layout/master', 'lokasi/lokasi_list', $data);
		}
	
	}
		
	public function update($id_lokasi)
	{
		$config['center'] = '-7.434778637847121, 109.24925812369965';
		$config['zoom'] = 15;
		$this->googlemaps->initialize($config);
		
			$marker['position']='-7.434778637847121, 109.24925812369965';
			$marker['draggable'] = true;
			$marker['ondragend'] = 'setToForm(event.latLng.lat(),event.latLng.lng())';
			$this->googlemaps->add_marker($marker);
		$value=$this->User_model->get_by_id($this->session->userdata('id'));	
		$row=$this->Lokasi_model->get_by_id($id_lokasi);
		if($row or $value){
			$data=array(
				'map'=> $this->googlemaps->create_map(),
				'button'=>'Simpan',
				'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
				'data_kab'=>$this->Kabupaten_model->get_all(),
				'data_propinsi'=>$this->Propinsi_model->get_all(),
				'action'=>site_url('lokasi/update_action'),
				'id_lokasi'=>set_value('id_lokasi', $row->id_lokasi),
				'id_kabupaten'=>set_value('id_kabupaten', $row->id_kabupaten),
				'id_propinsi'=>set_value('id_propinsi', $row->id_propinsi),
				'id_jenis'=>set_value('id_jenis', $row->id_jenis),
				'nama_kab'=>$this->Kabupaten_model->get_by_id($row->id_kabupaten)->nama_kab,
				'nama_prop'=>$this->Propinsi_model->get_by_id($row->id_propinsi)->nama_prop,	
				'nama_instansi'=>set_value('nama_instansi', $row->nama_instansi),			
				'jenis'=>$this->Jenis_lokasi_model->get_by_id($row->id_jenis)->jenis,
				'alamat'=>set_value('alamat', $row->alamat),
				'no_tlp'=>set_value('no_tlp', $row->no_tlp),
				'email'=>set_value('email', $row->email),
				'website'=>set_value('website', $row->website),
				'latitude'=>set_value('latitude', $row->latitude),
				'longitude'=>set_value('longitude', $row->longitude),
				'nama_user'=>set_value('nama', $value->nama),
				'foto'=>set_value('foto', $value->foto)
			);
			$this->template->load('layout/master', 'lokasi/lokasi_form', $data);
		}else{
			
			redirect(site_url('lokasi/index'));
		}
	}

	public function update_action()
	{
		$data=array(
		'id_lokasi'=>$this->input->post('id_lokasi'),
		'id_kabupaten'=>$this->input->post('id_kabupaten'),
		'id_propinsi'=>$this->input->post('id_propinsi'),
		'nama_instansi'=>$this->input->post('nama_instansi'),
		'id_jenis'=>$this->input->post('id_jenis'),
		'alamat'=>$this->input->post('alamat'),
		'no_tlp'=>$this->input->post('no_tlp'),
		'email'=>$this->input->post('email'),
		'website'=>$this->input->post('website'),
		'latitude'=>$this->input->post('latitude'),
		'longitude'=>$this->input->post('longitude')
		);
		$this->Lokasi_model->update($this->input->post('id_lokasi'), $data);
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');
		redirect(site_url('lokasi/index'));
	}
	
	public function create()
	{	
		$config['center'] = '-7.434778637847121, 109.24925812369965';
		$config['zoom'] = 15;
		$this->googlemaps->initialize($config);
		
			$marker['position']='-7.434778637847121, 109.24925812369965';
			$marker['draggable'] = true;
			$marker['ondragend'] = 'setToForm(event.latLng.lat(),event.latLng.lng())';
			$this->googlemaps->add_marker($marker);	
			$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_kab'=>$this->Kabupaten_model->get_all(),
		'data_propinsi'=>$this->Propinsi_model->get_all(),
		'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
		'button'=>'Tambah',
		'action'=>site_url('lokasi/create_action'),
		'id_lokasi'=>set_value('id_lokasi'),
		'nama_instansi'=>set_value('nama_instansi'),
		'id_jenis'=>set_value('id_jenis'),
		'alamat'=>set_value('alamat'),
		'no_tlp'=>set_value('no_tlp'),
		'email'=>set_value('email'),
		'website'=>set_value('website'),
		'id_kabupaten'=>set_value('id_kabupaten'),
		'id_propinsi'=>set_value('id_propinsi'),
		'latitude'=>set_value('latitude'),
		'longitude'=>set_value('longitude'),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto)
	);
        $this->template->load('layout/master', 'lokasi/lokasi_form', $data);		
	}
	
}

	public function create_action()
	{
		$data=array(		
		'id_lokasi'=>$this->input->post('id_lokasi'),
		'nama_instansi'=>$this->input->post('nama_instansi'),
		'id_jenis'=>$this->input->post('id_jenis'),
		'alamat'=>$this->input->post('alamat'),
		'no_tlp'=>$this->input->post('no_tlp'),
		'email'=>$this->input->post('email'),
		'website'=>$this->input->post('website'),
		'id_kabupaten'=>$this->input->post('id_kabupaten'),
		'id_propinsi'=>$this->input->post('id_propinsi'),
		'latitude'=>$this->input->post('latitude'),
		'longitude'=>$this->input->post('longitude')
		);
		$this->Lokasi_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('lokasi'));
	}

	

	public function delete($id_lokasi)
	{
		$row=$this->Lokasi_model->get_by_id($id_lokasi);
		if($row){
			$this->Lokasi_model->delete($row->id_lokasi);			
			redirect(site_url('lokasi'));
		}else{
			redirect(site_url('lokasi'));
		}
	}

	
}
