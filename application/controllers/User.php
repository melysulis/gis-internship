<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
        $this->load->library('googlemaps');
        $this->load->library('fungsi');
		$this->load->library('user_login');
        $this->load->model('User_model');
		$this->load->model('Auth_model');
	}

    public function index() {
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
        $data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_user'=>$this->User_model->get_all(),
        'action'=>site_url('user/create_action'),
		'id'=>set_value('id'),
		'nama'=>set_value('nama'),
        'username'=>set_value('username'),
        'password'=>set_value('password'),
        'level'=>set_value('level'),
        'phone'=>set_value('phone'),
        'foto'=>set_value('foto'),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto)
    );        
		if($this->session->userdata('level')=='2'){  
            redirect('dashboard');    			       
        }else{
			$this->template->load('layout/master', 'user/user_list', $data);
		}
	}
   }

   public function create_action()
	{
		$data=array(		
		'id'=>$this->input->post('id'),
		'nama'=>$this->input->post('nama'),
        'username'=>$this->input->post('username'),
        'password'=>$this->input->post('password'),
        'level'=>$this->input->post('level'),
        'phone'=>$this->input->post('phone'),
        'foto'=>$this->input->post('foto')
		);
		$this->User_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		User Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('user'));
	}

   public function my_profile() {
	$row=$this->User_model->get_by_id($this->session->userdata('id'));
	if($row){
	$data=array(			
		'map'=>$this->googlemaps->create_map(),	
		'button'=>'Simpan',
		'action'=>site_url('user/edit_profile_action'),
		'id'=>set_value('id', $row->id),
		'username'=>set_value('username', $row->username),
		'password'=>set_value('password', $row->password),
		'nama_user'=>set_value('nama', $row->nama),
		'phone'=>set_value('phone', $row->phone),
		'level'=>set_value('level', $row->level),
		'foto'=>set_value('foto', $row->foto)           

	);
    $this->template->load('layout/master', 'user/my_profile', $data);
	}else{
		redirect(site_url('user/my_profile'));
	}
    }

    public function edit_profile() {
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
        $data=array(			
            'map'=>$this->googlemaps->create_map(),	
            'button'=>'Simpan',
            'action'=>site_url('user/edit_profile_action'),
            'id'=>set_value('id', $row->id),
            'username'=>set_value('username', $row->username),
            'password'=>set_value('password', $row->password),
            'nama'=>set_value('nama', $row->nama),
            'phone'=>set_value('phone', $row->phone),
			'level'=>set_value('level', $row->level),
            'foto'=>set_value('foto', $row->foto),
			'nama_user'=>set_value('nama', $row->nama),           

        );
    $this->template->load('layout/master', 'user/edit_profile', $data);
    
        }else{
                     
			redirect(site_url('user/my_profile'));
        }
    
   }

    public function edit_profile_action() {
        $this->load->library('upload');
		$path ='./uploads/foto/';
		$config['upload_path']=$path;
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='2048';
		$config['max_width']='1024';
		$config['max_height']='768';
		$nama_file="foto_".time();
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);

		$id=$this->input->post('id');
		$gambar_lama=$this->input->post('ganti_gambar');
		if($_FILES['foto']['name'])
		{
			$field_name="foto";
			if($this->upload->do_upload($field_name))
			{
				$id=$this->input->post('id');		
		$nama=$this->input->post('nama');
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $level=$this->input->post('level');
        $phone=$this->input->post('phone');
        $foto=$this->upload->data();
			$user=([
				'id' => $id,
				'nama' => $nama,
				'username' => $username,
				'password' => $password,
				'level' => $level,
				'phone' => $phone,
				'foto' => $foto['file_name']			
			]);
			$data = array_merge($user);
			@unlink($path.$gambar_lama);
			$where = array('id'=>$id);
			if($this->User_model->update_profile($data, $where) == true)
			{
				$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Data Berhasil Diubah!
		  </div>');
			redirect(site_url('user/my_profile'));
			}
			else
			{
				$this->my_profile();
			}
			}
		}
		else
		{
			$data=array(				
				'id'=>$this->input->post('id'),
				'nama'=>$this->input->post('nama'),
                'username'=>$this->input->post('username'),
                'password'=>$this->input->post('password'),
                'phone'=>$this->input->post('phone'),
                'level'=>$this->input->post('level')
			);
		$this->User_model->update($this->input->post('id'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');	
		redirect(site_url('user/my_profile'));
		}
    }
    
    public function update_action()
	{		
			$data=array(				
				'id'=>$this->input->post('id'),
				'nama'=>$this->input->post('nama'),
                'username'=>$this->input->post('username'),
                'password'=>$this->input->post('password'),
                'phone'=>$this->input->post('phone'),
                'level'=>$this->input->post('level'),
                'foto'=>$this->input->post('foto')
			);
		$this->User_model->update($this->input->post('id'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Level User Berhasil Diubah!
	  </div>');	
		redirect(site_url('user'));
	}

    public function reset()
	{		
			$data=array(				
				'id'=>$this->input->post('id'),
				'nama'=>$this->input->post('nama'),
                'username'=>$this->input->post('username'),
                'password'=>$this->input->post('password'),
                'phone'=>$this->input->post('phone'),
                'level'=>$this->input->post('level'),
                'foto'=>$this->input->post('foto')
			);
		$this->User_model->update($this->input->post('id'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Password berhasil direset, silahkan login menggunakan password default!
	  </div>');	
		redirect(site_url('user'));
	}

    public function delete($id)
	{
		$row=$this->User_model->get_by_id($id);
		if($row){
			$this->User_model->delete($row->id);			
			redirect(site_url('user'));
		}else{
			redirect(site_url('user'));
		}
	}
    
}