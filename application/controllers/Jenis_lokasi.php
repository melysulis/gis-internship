<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_lokasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_lokasi_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	} 
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
			'action'=>site_url('jenis_lokasi/create_action'),
			'id_jenis'=>set_value('id_jenis'),
			'jenis'=>set_value('jenis'),
			'marker_jenis'=>set_value('marker_jenis'),
			'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
			'nama_user'=>set_value('nama', $row->nama),
			'foto'=>set_value('foto', $row->foto));
			$this->template->load('layout/master', 'jenis_lokasi/jenis_lokasi_list', $data);
		}
	}

	public function create_action()
	{
		$this->load->library('upload');
		$path ='./uploads/marker_jenis/';
		$config['upload_path']=$path;
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='2048';
		$config['max_width']='1024';
		$config['max_height']='768';
		$nama_file="marker_".time();
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);

		$id_jenis=$this->input->post('id_jenis');
		$gambar_lama=$this->input->post('ganti_gambar');
		if($_FILES['marker_jenis']['name'])
		{
			$field_name="marker_jenis";
			if($this->upload->do_upload($field_name))
			{
				$id_jenis=$this->input->post('id_jenis');
			$jenis=$this->input->post('jenis');
			$marker_jenis=$this->upload->data();
			$jenis=([
				'id_jenis' => $id_jenis,
				'jenis' => $jenis,
				'marker_jenis' => $marker_jenis['file_name']			
			]);
			$data = array_merge($jenis);
			@unlink($path.$gambar_lama);
			$this->Jenis_lokasi_model->insert($data);			
				$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Data Berhasil Ditambahkan!
		  </div>');
			redirect(site_url('jenis_lokasi'));			
		}
		else
		{
			$this->index();
		}
	}
}

	
	public function update_action()
	{
		$this->load->library('upload');
		$path ='./uploads/marker_jenis/';
		$config['upload_path']=$path;
		$config['allowed_types']='gif|jpg|png|jpeg';
		$config['max_size']='2048';
		$config['max_width']='1024';
		$config['max_height']='768';
		$nama_file="marker_".time();
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);

		$id_jenis=$this->input->post('id_jenis');
		$gambar_lama=$this->input->post('ganti_gambar');
		if($_FILES['marker_jenis']['name'])
		{
			$field_name="marker_jenis";
			if($this->upload->do_upload($field_name))
			{
				$id_jenis=$this->input->post('id_jenis');
			$jenis=$this->input->post('jenis');
			$marker_jenis=$this->upload->data();
			$jenis=([
				'id_jenis' => $id_jenis,
				'jenis' => $jenis,
				'marker_jenis' => $marker_jenis['file_name']			
			]);
			$data = array_merge($jenis);
			@unlink($path.$gambar_lama);
			$where = array('id_jenis'=>$id_jenis);
			if($this->Jenis_lokasi_model->upload($data, $where) == true)
			{
				$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Data Berhasil Diubah!
		  </div>');
			redirect(site_url('jenis_lokasi'));
			}
			else
			{
				$this->index();
			}
			}
		}
		else
		{
			$data=array(				
				'id_jenis'=>$this->input->post('id_jenis'),
				'jenis'=>$this->input->post('jenis'),
			);
		$this->Jenis_lokasi_model->update($this->input->post('id_jenis'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');	
		redirect(site_url('jenis_lokasi'));
		}
	}

	public function delete($id_jenis)
	{
		$row=$this->Jenis_lokasi_model->get_by_id($id_jenis);
		if($row){
			$this->Jenis_lokasi_model->delete($row->id_jenis);			
			redirect(site_url('jenis_lokasi'));
		}else{
			redirect(site_url('jenis_lokasi'));
		}
	}
}
