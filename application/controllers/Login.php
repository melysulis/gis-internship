<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

    public function index()
	{	
       		 $data=array(
			'action'=>site_url('login/aksi')
		);
		$this->load->view('front/login', $data);
	}

	public function aksi(){				
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->load->model('User_model');
		$this->User_model->ambillogin($username,$password);
						
	}
}    