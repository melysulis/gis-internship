<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_model');
		$this->load->model('Lokasi_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	} 
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_mhs'=>$this->Mahasiswa_model->get_all(),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto));
        $this->template->load('layout/master', 'mahasiswa/mhs_list', $data);
		}	
	}

		
	public function create()
	{		
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_lokasi'=>$this->Lokasi_model->get_all(),
		'button'=>'Tambah',
		'action'=>site_url('mahasiswa/create_action'),
		'id_mhs'=>set_value('id_mhs'),
		'nim'=>set_value('nim'),
		'nama_mhs'=>set_value('nama'),
		'jenis_kelamin'=>set_value('jenis_kelamin'),
		'angkatan'=>set_value('angkatan'),
		'alamat'=>set_value('alamat'),
		'no_tlp'=>set_value('no_tlp'),
		'id_lokasi'=>set_value('id_lokasi'),
		'tgl_mulaiPKL'=>set_value('tgl_mulaiPKL'),
		'tgl_selesaiPKL'=>set_value('tgl_selesaiPKL'),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto)
	);
        $this->template->load('layout/master', 'mahasiswa/mhs_form', $data);
	}
	
}

	public function create_action()
	{
		$data=array(		
		'id_mhs'=>$this->input->post('id_mhs'),
		'nim'=>$this->input->post('nim'),
		'nama'=>$this->input->post('nama'),
		'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
		'angkatan'=>$this->input->post('angkatan'),
		'id_lokasi'=>$this->input->post('id_lokasi'),
		'tgl_mulaiPKL'=>$this->input->post('tgl_mulaiPKL'),
		'tgl_selesaiPKL'=>$this->input->post('tgl_selesaiPKL')
		);
		$this->Mahasiswa_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('mahasiswa'));
	}

	public function update($id_mhs)
	{
		$value=$this->User_model->get_by_id($this->session->userdata('id'));
		$row=$this->Mahasiswa_model->get_by_id($id_mhs);
		if($row or $value){
			$data=array(
				'map'=> $this->googlemaps->create_map(),
				'data_lokasi'=>$this->Lokasi_model->get_all(),
				'button'=>'Simpan',
				'action'=>site_url('mahasiswa/update_action'),
				'id_mhs'=>set_value('id_mhs', $row->id_mhs),
				'nim'=>set_value('nim', $row->nim),
				'nama_mhs'=>set_value('nama', $row->nama),
				'jenis_kelamin'=>set_value('jenis_kelamin', $row->jenis_kelamin),
				'angkatan'=>set_value('angkatan', $row->angkatan),
				'alamat'=>set_value('alamat', $row->alamat),
				'no_tlp'=>set_value('no_tlp', $row->no_tlp),
				'id_lokasi'=>set_value('id_lokasi', $row->id_lokasi),
				'tgl_mulaiPKL'=>set_value('tgl_mulaiPKL', $row->tgl_mulaiPKL),
				'tgl_selesaiPKL'=>set_value('tgl_selesaiPKL', $row->tgl_selesaiPKL),
				'nama_user'=>set_value('nama', $value->nama),
		'foto'=>set_value('foto', $value->foto)
			);
			$this->template->load('layout/master', 'mahasiswa/mhs_form', $data);			
		}else{			
			redirect(site_url('mahasiswa'));
		}
		
	}

	public function update_action()
	{
		$data=array(
			'id_mhs'=>$this->input->post('id_mhs'),
			'nim'=>$this->input->post('nim'),
			'nama'=>$this->input->post('nama'),
			'jenis_kelamin'=>$this->input->post('jenis_kelamin'),
			'angkatan'=>$this->input->post('angkatan'),
			'alamat'=>$this->input->post('alamat'),
			'no_tlp'=>$this->input->post('no_tlp'),
			'id_lokasi'=>$this->input->post('id_lokasi'),
			'tgl_mulaiPKL'=>$this->input->post('tgl_mulaiPKL'),
			'tgl_selesaiPKL'=>$this->input->post('tgl_selesaiPKL')
		);
		$this->Mahasiswa_model->update($this->input->post('id_mhs'), $data);
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');
		redirect(site_url('mahasiswa'));
	}

	public function delete($id_mhs)
	{
		$row=$this->Mahasiswa_model->get_by_id($id_mhs);
		if($row){
			$this->Mahasiswa_model->delete($row->id_mhs);			
			redirect(site_url('mahasiswa'));
		}else{
			redirect(site_url('mahasiswa'));
		}
	}
	
}
