<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propinsi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Propinsi_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	} 
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
        'action'=>site_url('propinsi/create_action'),
		'id_propinsi'=>set_value('id_propinsi'),
		'nama_prop'=>set_value('nama_prop'),
            'data_propinsi'=>$this->Propinsi_model->get_all(),
			'nama_user'=>set_value('nama', $row->nama),
			'foto'=>set_value('foto', $row->foto));
		$this->template->load('layout/master', 'propinsi/propinsi_list', $data);
		}
	
	}
	
	
	public function create_action()
	{
		$data=array(		
		'id_propinsi'=>$this->input->post('id_propinsi'),
		'nama_prop'=>$this->input->post('nama_prop')
		);
		$this->Propinsi_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('propinsi'));
	}

	
	public function update_action()
	{		
			$data=array(				
				'id_propinsi'=>$this->input->post('id_propinsi'),
				'nama_prop'=>$this->input->post('nama_prop')
			);
		$this->Propinsi_model->update($this->input->post('id_propinsi'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');	
		redirect(site_url('propinsi'));
	}

	public function delete($id_propinsi)
	{
		$row=$this->Propinsi_model->get_by_id($id_propinsi);
		if($row){
			$this->Propinsi_model->delete($row->id_propinsi);			
			redirect(site_url('propinsi'));
		}else{
			redirect(site_url('propinsi'));
		}
	}
	
}
