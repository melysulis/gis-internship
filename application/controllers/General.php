<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Options_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	}

	public function index(){
		$row=$this->User_model->get_by_id($this->session->userdata('id'));		
		$a=$this->Options_model->get_by_id(2);
		$b=$this->Options_model->get_by_id(5);
		$c=$this->Options_model->get_by_id(7);
		if($a or $b or $c or $row){
			$data=array(
			'map'=> $this->googlemaps->create_map(),
			'button'=>'Simpan',
			'id_title'=>set_value('id_options', $a->id_options),
			'id_caption'=>set_value('id_options', $b->id_options),
			'name_title'=>set_value('option_name', $a->option_name),			
			'name_caption'=>set_value('option_name', $b->option_name),
			'title'=>set_value('option_value', $a->option_value),
			'caption'=>set_value('option_value', $b->option_value),
			'id_nama_web'=>set_value('id_options', $c->id_options),
			'nama_web'=>set_value('option_name', $c->option_name),
			'nama_web_value'=>set_value('option_value', $c->option_value),
			'nama_user'=>set_value('nama', $row->nama),
			'foto'=>set_value('foto', $row->foto),
		);
		$this->template->load('layout/master', 'options/general', $data);
		}else{
			
			redirect(site_url('general'));
		}
	}

	public function update()
	{				
		$data=array(	
			'id_options' => $this->input->post('id_options'),
			'option_name' => $this->input->post('option_name'),
			'option_value' => $this->input->post('option_value')
			);
			$this->Options_model->update($this->input->post('id_options'), $data);	
            $this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Berhasil Mengubah General!
	  </div>');					
			redirect(site_url('general'));				
	}
		
    
}
