<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Options_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	}
			
    public function index()
	{
		$value=$this->User_model->get_by_id($this->session->userdata('id'));
        $row=$this->Options_model->get_by_id(1);
		if($row or $value){
			$data=array(
				'map'=> $this->googlemaps->create_map(),				
				'button'=>'Simpan',
				'action'=>site_url('tentang/update'),
				'id_options'=>set_value('id_options', $row->id_options),
				'option_name'=>set_value('option_name', $row->option_name),
				'option_value'=>set_value('option_value', $row->option_value),
				'footer'=>$this->Options_model->get_by_id(6),
				'nama_user'=>set_value('nama', $value->nama),
		'foto'=>set_value('foto', $value->foto)
			);
			$this->template->load('layout/master', 'options/tentang', $data);
		}else{
			
			redirect(site_url('tentang'));
		}
	}

    public function update()
	{
		$data=array(
        'id_options'=>$this->input->post('id_options'),
        'option_name'=>$this->input->post('option_name'),
        'option_value'=>$this->input->post('option_value')

		);
		$this->Options_model->update($this->input->post('id_options'), $data);
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Berhasil Mengubah Tentang!
	  </div>');
		redirect(site_url('tentang'));
	}
}
