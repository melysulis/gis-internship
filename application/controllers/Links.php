<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Links extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Links_model');
		$this->load->library('googlemaps');
		$this->load->model('User_model');
	} 
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
        'action'=>site_url('links/create_action'),
		'id_link'=>set_value('id_link'),
		'nama_link'=>set_value('nama_link'),
        'url'=>set_value('url'),
            'data_link'=>$this->Links_model->get_all(),
			'nama_user'=>set_value('nama', $row->nama),
			'foto'=>set_value('foto', $row->foto));
		$this->template->load('layout/master', 'links/links_list', $data);
		}	
	}
	
	
	public function create_action()
	{
		$data=array(		
		'id_link'=>$this->input->post('id_link'),
		'nama_link'=>$this->input->post('nama_link'),
        'url'=>$this->input->post('url')
		);
		$this->Links_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('links'));
	}

	
	public function update_action()
	{		
			$data=array(				
				'id_link'=>$this->input->post('id_link'),
				'nama_link'=>$this->input->post('nama_link'),
                'url'=>$this->input->post('url')
			);
		$this->Links_model->update($this->input->post('id_link'), $data);	
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');	
		redirect(site_url('links'));
	}

	public function delete($id_link)
	{
		$row=$this->Links_model->get_by_id($id_link);
		if($row){
			$this->Links_model->delete($row->id_link);			
			redirect(site_url('links'));
		}else{
			redirect(site_url('links'));
		}
	}
	
}
