<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

    public function index()
	{	       		
		$this->load->view('front/login');
	}

    public function login_user($username, $password)
	{	
       		$this->form_validation->set_rules('username', 'username', 'required', array('required' => '%s Harus Diisi!!!!'));
            $this->form_validation->set_rules('password', 'password', 'required', array('required' => '%s Harus Diisi!!!!'));
            
            if($this->form_validation->run() == TRUE){
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $this->user_login->login($username, $password);
            }  else{    
                                                          
                redirect('auth');                
            }
	}

    
    public function logout_user()
    {	
	$this->user_login->logout();
    }

    
}    