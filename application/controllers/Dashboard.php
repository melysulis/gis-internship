<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_model');
		$this->load->model('Lokasi_model');
		$this->load->model('Jenis_lokasi_model');
		$this->load->model('Propinsi_model');
		$this->load->model('Kabupaten_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	}

	public function index()
	{	$keyword=$this->input->post('keyword');
		$kab=$this->Kabupaten_model->get_kab($keyword);
		foreach ($kab as $key => $row) {
			if(!$keyword ){
				$config['center'] = '-7.3325227161676985, 109.41400139320258';
			}		else{
				$config['center'] = "$row->latitude,$row->longitude";
			}
		}
		$config['zoom'] = 11;
		$this->googlemaps->initialize($config);
		
		$lokasi=$this->Lokasi_model->get_all();
			foreach ($lokasi as $key => $value) {
			$marker=array();
			$marker['position']="$value->latitude,$value->longitude";
			$marker['animation']="DROP";
			$marker['infowindow_content'] = '<div class="media" style="width:250px;">';
			$marker['infowindow_content'] .= '<div class="media-body">';
			$marker['infowindow_content'] .= '<h5> '.$value->nama_instansi.'</h5>';
			$marker['infowindow_content'] .= '<img src=" '.base_url('uploads/' .$value->icon).'" width="50%">';
			$marker['infowindow_content'] .= '<br> </br>';
			$marker['infowindow_content'] .= '<p>Alamat : '.$value->alamat.'</p>';
			$marker['infowindow_content'] .= '<p>No Tlp : '.$value->no_tlp.'</p>';
			$marker['infowindow_content'] .= '<p>Email : '.$value->email.'</p>';
			$marker['infowindow_content'] .= '<a href=" '.$value->website.' "> <p>Website : '.$value->website.'</p>';
			$marker['infowindow_content'] .= '</div>';
			$marker['infowindow_content'] .= '</div>';
			$this->googlemaps->add_marker($marker);
		}
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data = array(
			'map'=> $this->googlemaps->create_map(),
			'data_kab'=>$this->Kabupaten_model->get_all(),
			'data_lokasi'=>$this->Lokasi_model->get_all(),
			'data_jenis'=>$this->Jenis_lokasi_model->get_all(),
		'count_mhs'=>$this->Mahasiswa_model->get_count(),
		'count_lokasi'=>$this->Lokasi_model->get_count(),
		'count_prop'=>$this->Propinsi_model->get_count(),
		'count_kab'=>$this->Kabupaten_model->get_count(),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto),
		);	
		$this->Kabupaten_model->get_kab($this->input->post('keyword'), $data);
		$this->template->load('layout/master', 'dashboard/dashboard', $data);
		}
	}

	
}
