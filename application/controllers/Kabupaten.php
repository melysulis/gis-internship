<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kabupaten_model');
		$this->load->model('Propinsi_model');
		$this->load->model('User_model');
		$this->load->library('googlemaps');
	} 
	
	public function index()
	{
		$row=$this->User_model->get_by_id($this->session->userdata('id'));
		if($row){
		$data=array(
			'map'=> $this->googlemaps->create_map(),
		'data_kab'=>$this->Kabupaten_model->get_all(),
		'data_prop'=>$this->Propinsi_model->get_all(),
		'action'=>site_url('kabupaten/create_action'),
		'id_kabupaten'=>set_value('id_kabupaten'),
		'nama_kab'=>set_value('nama_kab'),
		'id_propinsi'=>set_value('id_propinsi'),
		'nama_user'=>set_value('nama', $row->nama),
		'foto'=>set_value('foto', $row->foto)
	);
        $this->template->load('layout/master', 'kabupaten/kab_list', $data);
	}	
	}

	public function create_action()
	{
		$data=array(		
		'id_kabupaten'=>$this->input->post('id_kabupaten'),
		'nama_kab'=>$this->input->post('nama_kab'),
		'id_propinsi'=>$this->input->post('id_propinsi')
		);
		$this->Kabupaten_model->insert($data);
		$this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Ditambahkan!
	  </div>');
		redirect(site_url('kabupaten'));
	}

	
	public function update_action()
	{
		$data=array(
			'id_kabupaten'=>$this->input->post('id_kabupaten'),
			'nama_kab'=>$this->input->post('nama_kab'),
			'id_propinsi'=>$this->input->post('id_propinsi')
		);
		$this->Kabupaten_model->update($this->input->post('id_kabupaten'), $data);
		$this->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Data Berhasil Diubah!
	  </div>');
		redirect(site_url('kabupaten'));
	}

	
	public function delete($id_kabupaten)
	{
		$row=$this->Kabupaten_model->get_by_id($id_kabupaten);
		if($row){
			$this->Kabupaten_model->delete($row->id_kabupaten);			
			redirect(site_url('kabupaten'));
		}else{
			redirect(site_url('kabupaten'));
		}
	}
	
}
