<div class="content-header">
      <div class="container-fluid">
      <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-building"> Data Jenis Instansi </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Jenis Instansi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus"></i> Tambah Jenis Instansi</button>
                <table id="data_jenis" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Jenis Instansi</th>
                    <th>Icon Marker</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_jenis as $value): ?>
                  <tr align="center">
                  <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->jenis ?></td>
                    <td><img src="<?= base_url() . '/uploads/marker_jenis/' . $value->marker_jenis?>" class="img-thumbnail center-block" border="1" width="25%"></a></td>
                    <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?=$value->id_jenis ?>" ><i class="fa fa-edit"></i></button>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('jenis_lokasi/delete/'.$value->id_jenis) ?>"><i class='fa fa-trash'></i></a>
                  </td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
                <strong><a href="https://mapicons.mapsmarker.com/">Marker by Map Icons.</a></strong>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

 <div class="modal fade" id="modal-tambah">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h4 class="modal-title">Tambah Jenis Instansi</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form style="padding: 15px;" action="<?= site_url("jenis_lokasi/create_action") ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
          
						<div class="row">
							<label for="varchar">Jenis Instansi</label>
							<input type="text" class="form-control" name="jenis" id="jenis"  required />
						</div>
					</div>

          <div class="form-group">
                    <label for="icon" class="col-sm-6 col-form-label">Icon Marker</label>
                    <div class="col-sm-12">
                      <input type="file" class="form-control" name="marker_jenis" required>
                    </div>
                  </div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_jenis" value="<?= $id_jenis ?>">
              
							<button type="submit" class="btn btn-primary btn-block "><i class="fas fa-plus"></i> Tambah</button>
						</div>
					</div>
          
				</form>
			</div>
		</div>
	</div>
</div>

<?php  $no = 0;
                  foreach($data_jenis as $value): $no++; ?>
<div class="modal fade" id="modal-edit<?=$value->id_jenis ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title fa fa-edit"> Ubah Jenis Lokasi</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form style="padding: 15px;"  method="POST" action="<?= site_url("jenis_lokasi/update_action") ?>" enctype="multipart/form-data">
					<div class="form-group">
          
						<div class="row">
							<label for="varchar">Jenis Lokasi</label>
							<input type="text" class="form-control" name="jenis" id="jenis" value="<?=$value->jenis?>"  required />
						</div>
					</div>

          <div class="form-group">
                    <label for="icon" class="col-sm-6 col-form-label">Icon Marker</label>
                    <div class="col-sm-12">                    
                      <input type="file" class="form-control" name="marker_jenis" >
                      <img src="<?= base_url() . '/uploads/marker_jenis/' . $value->marker_jenis?>" width="100">
                    </div>
                  </div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_jenis" value="<?=$value->id_jenis?>">
                        <input type="hidden" name="gambar_lama" value="<?=$value->marker_jenis?>">
              
                        <button type="submit" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Simpan </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>

<script>
  $(function () {
    $("#data_jenis").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>



<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data jenis instansi akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
  }
})
});
</script>