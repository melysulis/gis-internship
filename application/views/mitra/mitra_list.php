<div class="content-header">
      <div class="container-fluid">
      <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-building"> Data Mitra</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Mitra</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                             
              <table id="data_mitra" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama Instansi</th>
                    <th>Icon</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_mitra as $value): ?>
                  <tr align="center">
                  <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->nama_instansi ?></td>
                    <td><img src="<?= base_url() . '/uploads/' . $value->icon?>" class="img-thumbnail center-block" border="1" width="25%"></a></td>
                    <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?=$value->id_lokasi ?>" ><i class="fa fa-edit"> Edit Icon</i></button>
                  </td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

<?php  $no = 0;
     foreach($data_mitra as $value): $no++; ?>
<div class="modal fade" id="modal-edit<?=$value->id_lokasi?>" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title fa fa-edit"> Ubah Icon</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" method="POST" action="<?= site_url('mitra/update') ?>" enctype="multipart/form-data">
        <div class="form-group row">
                    <label for="icon" class="col-sm-2 col-form-label">Nama Instansi</label>
                    <div class="col-sm-10">
                      <input readonly type="text" class="form-control" name="nama_instansi" value="<?=$value->nama_instansi?>">
                    </div>
                  </div> 
                  <div class="form-group row">
                    <label for="icon" class="col-sm-2 col-form-label">Icon</label>
                    <div class="col-sm-10">
                    <img src="<?= base_url() . '/uploads/' . $value->icon?>" width="100">
                      <input type="file" class="form-control" name="icon">
                    </div>
                  </div>

					<div class="form-group">
						<div class="row">
            <input type="hidden" name="gambar_lama" value="<?=$value->icon?>">
                <input type="hidden" name="id_lokasi" value="<?=$value->id_lokasi?>">
                <input type="hidden" name="id_jenis" value="<?=$value->id_jenis?>">
                <input type="hidden" name="id_propinsi" value="<?=$value->id_propinsi?>">
                <input type="hidden" name="id_kabupaten" value="<?=$value->id_kabupaten?>">
                <input type="hidden" name="alamat" value="<?=$value->alamat?>">
                <input type="hidden" name="no_tlp" value="<?=$value->no_tlp?>">
                <input type="hidden" name="email" value="<?=$value->email?>">
                <input type="hidden" name="website" value="<?=$value->website?>">
                <input type="hidden" name="longitude" value="<?=$value->longitude?>">
                <input type="hidden" name="latitude" value="<?=$value->latitude?>">
							<button type="submit" class="btn btn-warning btn-block "><i class="fas fa-save"></i> Simpan </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>

<script>
  $(function () {
    $("#data_mitra").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>