<div class="content-header">
	<div class="container-fluid">
  <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark fa fa-link"> Data Link </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Link</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus"></i> Tambah Link</button>
                <table id="data_link" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>Nomor</th>
                    <th>Nama Link</th>
                    <th>URL</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_link as $value): ?>
                  <tr align="center">
                    <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->nama_link ?></td>
                    <td><a href="<?php echo $value->url ?>"><?php echo $value->url ?></a></td>
                    <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?=$value->id_link ?>" ><i class="fa fa-edit"></i></button>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('links/delete/'.$value->id_link) ?>"><i class='fa fa-trash'></i></a>
                     </td>                
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

<div class="modal fade" id="modal-tambah" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h4 class="modal-title">Tambah Link</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" method="POST" action="<?= site_url('links/create_action') ?>" enctype="multipart/form-data">
					<div class="form-group">          
						<div class="row">
							<label for="varchar">Nama Link</label>
							<input type="text" class="form-control" name="nama_link" id="nama_link" value="<?=$nama_link?>" required />
						</div>
					</div>
                    <div class="form-group">          
						<div class="row">
							<label for="varchar">URL</label>
							<input type="text" class="form-control" name="url" id="url" value="<?=$url?>" required />
						</div>
					</div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_link" value="<?= $id_link ?>">
              
							<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-plus"></i> Tambah</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php  $no = 0;
                  foreach($data_link as $value): $no++; ?>
<div class="modal fade" id="modal-edit<?=$value->id_link?>" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title fa fa-edit"> Ubah Link</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" method="POST" action="<?= site_url('links/update_action') ?>" enctype="multipart/form-data">
					<div class="form-group">          
						<div class="row">
							<label for="varchar">Nama Link</label>
							<input type="text" class="form-control" name="nama_link" id="nama_link" value="<?=$value->nama_link?>" required />
						</div>
					</div>
                    <div class="form-group">          
						<div class="row">
							<label for="varchar">URL</label>
							<input type="text" class="form-control" name="url" id="url" value="<?=$value->url?>" required />
						</div>
					</div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_link" value="<?= $value->id_link ?>">
              
							<button type="submit" class="btn btn-warning btn-block "><i class="fas fa-save"></i> Simpan </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>

<script>
  $(function () {
    $("#data_link").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>

<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data link akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
      
  }
})
});
</script>