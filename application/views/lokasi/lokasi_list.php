<div class="content-header">
      <div class="container-fluid">
      <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-building"> Data Instansi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Instansi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                              
              <a class="mb-3 btn btn-primary" href="<?= site_url('lokasi/create') ?>"><i class="fa fa-plus"></i> Tambah Instansi</a>
             
              <table id="data_lokasi" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama Instansi</th>
                    <th>Jenis</th>
                    <th>Alamat</th>
                    <th>Kab/Kota</th>
                    <th>Provinsi</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_lokasi as $value): ?>
                  <tr align="center">
                  <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->nama_instansi ?></td>
                    <td><?php echo $value->jenis ?></td>
                    <td><?php echo $value->alamat ?></td>
                    <td><?php echo $value->nama_kab ?></td>
                    <td><?php echo $value->nama_prop ?></td>
                    <td><?php echo $value->latitude ?></td>
                    <td><?php echo $value->longitude ?></td>
                    <td>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-detail<?=$value->id_lokasi ?>" ><i class="fa fa-eye"></i></button>
                    <a class="mb-3 btn btn-warning" href="<?= site_url('lokasi/update/'.$value->id_lokasi) ?>"><i class='fa fa-edit'></i></a>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('lokasi/delete/'.$value->id_lokasi) ?>"><i class='fa fa-trash'></i></a>
                  </td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

        <?php  $no = 0;
                  foreach($data_lokasi as $value): $no++; ?>
<div class="modal fade" id="modal-detail<?=$value->id_lokasi?>" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h4 class="modal-title fa fa-eye"> Detail Instansi</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" >
					<div class="form-group">
          
          <table class="table table-striped">
          <input type="hidden" name="id_lokasi" value="<?=$value->id_lokasi?>">
						<tr>
							<td width="20%"><b>Nama Instansi</b></td>
							<td><?= $value->nama_instansi ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Jenis</b></td>
							<td><?= $value->jenis ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Alamat</b></td>
							<td><?= $value->alamat ?></td>
						</tr>
						<tr>
							<td width="20%"><b>No Tlp</b></td>
							<td><?= $value->no_tlp ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Email</b></td>
							<td><?= $value->email ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Website</b></td>
							<td><?= $value->website ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Kab/Kota</b></td>
							<td><?= $value->nama_kab ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Provinsi</b></td>
							<td><?= $value->nama_prop ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Latitude</b></td>
							<td><?= $value->latitude ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Longitude</b></td>
							<td><?= $value->longitude ?></td>
						</tr>
					</table>
					<a href="<?= site_url('lokasi/update/' . $value->id_lokasi) ?>" class="btn btn-warning">
						<i class="fa fa-edit"></i> Ubah 
					</a>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>        
        

<script>
  $(function () {
    $("#data_lokasi").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>

<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data instansi akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
      
  }
})
});
</script>