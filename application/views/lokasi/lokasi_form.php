<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark "> Form Data Instansi </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="<?= site_url("lokasi")?>">Data Instansi</a></li>
              <li class="breadcrumb-item active">Form Data Instansi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="row">
<div class="col-md-12">
<div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Form Data Instansi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="POST" action="<?=$action?>">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nim" class="col-sm-2 col-form-label">Nama </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama_instansi" name="nama_instansi" value="<?=$nama_instansi?>" required>
                    </div>
                  </div>
                  
                  <div class="form-group row" >
                        <label for="alamat" class="col-sm-2 col-form-label">Jenis</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4"  name="id_jenis" required>
						  <option></option>
						  <?php foreach($data_jenis as $value):?>
							<option <?=$id_jenis == $value->id_jenis?"selected":''?> value="<?= $value->id_jenis?>"><?= $value->jenis?></option>

							<?php endforeach?>						  						  
                        </select>
                        </div></div>
                    
                  <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="alamat" name="alamat" required><?=$alamat?></textarea>
                    </div>
                  </div>

                  <div class="form-group row" >
                        <label for="alamat" class="col-sm-2 col-form-label">Kab/Kota</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4"  name="id_kabupaten" required>
						  <option></option>
						  <?php foreach($data_kab as $value):?>
							<option <?=$id_kabupaten == $value->id_kabupaten?"selected":''?> value="<?= $value->id_kabupaten?>"><?= $value->nama_kab?></option>

							<?php endforeach?>						  						  
                        </select>
                        </div></div>

                        <div class="form-group row" >
                        <label for="alamat" class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4"  name="id_propinsi" required>
						  <option></option>
						  <?php foreach($data_propinsi as $row):?>
                <option <?=$id_propinsi == $row->id_propinsi?"selected":''?> value="<?= $row->id_propinsi?>"><?= $row->nama_prop?></option>

							<?php endforeach?>						  						  
                        </select>
                        </div></div>

                                    
                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Lat</label>                    
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="latitude" name="latitude" value="<?=$latitude?>" required>   
                      </div>                 
                  </div>       
                  <div class="form-group row">
                    <label for="website" class="col-sm-2 col-form-label">Long</label>                    
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="longitude" name="longitude" value="<?=$longitude?>" required>   
                      </div>                 
                  </div>

                  <div class="form-group row">
                    <label for="telepon" class="col-sm-2 col-form-label">Telepon</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="no_tlp" name="no_tlp" value="<?=$no_tlp?>">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>                    
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="email" name="email" value="<?=$email?>" >   
                      </div>                 
                  </div>       
                  <div class="form-group row">
                    <label for="website" class="col-sm-2 col-form-label">URL Website</label>                    
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="website" name="website" value="<?=$website?>" >   
                      </div>                 
                  </div>     

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <input type="hidden" name="id_lokasi" value="<?=$id_lokasi?>">
                  <button type="submit" class="btn btn-info"><?=$button?></button>
                  <button class="btn btn-default float-right"><a href="<?= site_url('lokasi') ?>">Kembali</a></button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div></div>

            <div class="col-md-6">

              </div>
              </div>