<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark "> Tentang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Tentang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="row">
	<div class="col-md-12">
    <?= $this->session->flashdata('message');?>
		<div class="card card-primary">
			<div class="card-header">
				<div class="card-title">
					<i class="fa fa-tasks"></i> Ubah Tentang
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body m-3">
            <form class="form-horizontal" method="POST" action="<?=$action?>">
                <div class="card-body">
				<div class="form-group" >
					<div class="row">
						<label class="col-md-2" for="varchar">Judul</label>
						<div class="col-md-6">
                            <input type="text" class="form-control" name="option_name" id="option_name" value="<?= $option_name ?>">
                           
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Deskripsi</label>
						<div class="col-md-10">
                        <textarea name="option_value"  id="option_value" rows="5" class="form-control"><?= $option_value ?></textarea>
						</div>
					</div>
				</div>
                </div>

                <div class="card-footer">
                    
							<input type="hidden" name="id_options" id="id_options" value="<?= $id_options ?>">
                <button type="submit" class="btn btn-info"><?=$button?></button>
                </div>
            </form>
			</div>
		</div>

		<div class="card card-primary">
			<div class="card-header">
				<div class="card-title">
					<i class="fa fa-tasks"></i> Ubah Deskripsi Footer
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body m-3">
            <form class="form-horizontal" method="POST" action="<?= $action ?>">
                <div class="card-body">
				
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Deskripsi</label>
						<div class="col-md-10">
						<input type="hidden" name="id_options" id="id_options" value="<?= $footer->id_options ?>">
						<input type="hidden" name="option_name" id="option_name" value="<?= $footer->option_name ?>">
                        <textarea name="option_value" id="option_value" rows="5" class="form-control"><?= $footer->option_value ?></textarea>
						</div>
					</div>
				</div>
                </div>

                <div class="card-footer">
                    
							<input type="hidden" name="id_options" id="id_options" value="<?= $footer->id_options ?>">
                <button type="submit" class="btn btn-info"><?=$button?></button>
                </div>
            </form>
			</div>			

		</div>

	</div>
</div>

