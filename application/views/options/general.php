<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark "> General</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">General</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="row">
	<div class="col-md-12">
    <?= $this->session->flashdata('message');?>
	<div class="card card-primary">
			<div class="card-header">
				<div class="card-title">
					<i class="fa fa-tasks"></i> Ubah Nama Web
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body m-3">
            <form class="form-horizontal" method="POST" action="<?= site_url('general/update') ?>">
                <div class="card-body">
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Nama Web</label>
						<div class="col-md-10">
						<input type="hidden" name="option_name" id="option_name" value="<?= $nama_web?>">
                        <input type="text" class="form-control" name="option_value" id="option_value" value="<?= $nama_web_value?>">
						</div>
					</div>
				</div>				
                </div>

                <div class="card-footer">
				<input type="hidden" name="id_options" id="id_options" value="<?= $id_nama_web?>">
                <button type="submit" class="btn btn-info"><?=$button?></button>
                </div>
            </form>
			</div>

			

		</div>

		<div class="card card-primary">
			<div class="card-header">
				<div class="card-title">
					<i class="fa fa-tasks"></i> Ubah Title
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body m-3">
            <form class="form-horizontal" method="POST" action="<?= site_url('general/update') ?>">
                <div class="card-body">
				
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Title</label>
						<div class="col-md-10">
                        <input type="text" class="form-control" name="option_name" id="option_name" value="<?= $name_title?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Sub Title</label>
						<div class="col-md-10">
                        <input type="text" class="form-control" name="option_value" id="option_value" value="<?= $title?>">
						</div>
					</div>
				</div>				
                </div>

                <div class="card-footer">
				<input type="hidden" name="id_options" id="id_options" value="<?= $id_title?>">
                <button type="submit" class="btn btn-info"><?=$button?></button>
                </div>
            </form>
			</div>

			

		</div>

		<div class="card card-primary">
			<div class="card-header">
				<div class="card-title">
					<i class="fa fa-tasks"></i> Ubah Caption
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body m-3">
            <form class="form-horizontal" method="POST" action="<?= site_url('general/update') ?>">
                <div class="card-body">			
				
				<div class="form-group">
					<div class="row">
						<label class="col-md-2" for="varchar">Caption</label>
						<div class="col-md-10">
                        <textarea name="option_value" id="option_value" rows="5" class="form-control"><?= $caption ?></textarea>
						</div>
					</div>
				</div>				
                </div>

                <div class="card-footer">
				<input type="hidden" name="id_options" id="id_options" value="<?= $id_caption?>">
				<input type="hidden" name="option_name" id="option_name" value="<?= $name_caption?>">
                <button type="submit" class="btn btn-info"><?=$button?></button>
                </div>
            </form>
			</div>

			

		</div>

	</div>
</div>

