<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark "> Form Ubah Profil </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="<?= site_url("user/my_profile")?>">Profil Saya</a></li>
              <li class="breadcrumb-item active">Form Ubah Profil</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="row">
	<div class="col-md-12">  
		<div class="card card-warning">
			<div class="card-header">
				<div class="card-title">
					<b><i class="fa fa-edit"></i> Ubah Profil Saya</b>
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body">
				<div style="padding: 15px;">
                <form class="form-horizontal" method="POST" action="<?=$action?>" enctype="multipart/form-data">
                <div class="card-body">

                <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama" name="nama" value="<?=$nama?>" >
                    </div>
                  </div>

                  <div class="form-group row">   
                    <label for="" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="username" name="username" value="<?=$username?>" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" value="<?=$password?>" >
                    </div>
                  </div>

                  <div class="form-group row">  
                  <label for="" class="col-sm-2 col-form-label">No Tlp</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="phone" name="phone" value="<?=$phone?>" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label">Foto</label>
                    <div class="col-sm-10">                      
                      <input type="file" class="form-control" name="foto" >
                      <img src="<?= base_url() . '/uploads/foto/' . $foto?>" width="100">
                    </div>
                  </div>
                  
                  <div class="card-footer">
                  <input type="hidden" name="id" value="<?=$id?>">
                  <input type="hidden" name="level" value="<?=$level?>">
                  <input type="hidden" name="gambar_lama" value="<?=$foto?>">
                  <button type="submit" class="btn btn-info"><?=$button?></button>
                  <button class="btn btn-default float-right"><a href="<?= site_url('user/my_profile') ?>">Kembali</a></button>
                </div>
                  </form>
                  
				</div>
			</div>
		</div>
	</div>
</div>

