<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-user"> Profil Saya </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item ">Profil Saya</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="row">
	<div class="col-md-10">
    <?= $this->session->flashdata('message');?>
		<div class="card card-success">
			<div class="card-header">
				<div class="card-title">
					<b><i class="fa fa-user"></i> My Profile</b>
				</div>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
				</div>
			</div>
			<div class="card-body">
				<div style="padding: 15px;">
					<table class="table table-striped" >
                    <tr >
                    <td>
                    <a href="<?= base_url('uploads/foto/' . $foto) ?>" data-toggle="lightbox"><img src="<?= base_url('uploads/foto/' . $foto) ?>" class="img-thumbnail center-block" alt="foto" width="200%"></a>
						</td>
                        </tr>
						<tr>
							<td width="20%"><b>Username</b></td>
							<td><?=$username?></td>
						</tr>
                        <tr>
							<td width="20%"><b>Name</b></td>
							<td><?=$nama_user?></td>
						</tr>
						<tr>
							<td width="20%"><b>Phone</b></td>
							<td><?=$phone?></td>
						</tr>
					</table>
          <a href="<?= site_url('user/edit_profile') ?>" class="btn btn-warning">
          <i class="fa fa-edit"> Ubah</a></i>  
          
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

