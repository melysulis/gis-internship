<div class="content-header">
	<div class="container-fluid">
  <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark fas fa-user"> Data User </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus"></i> Tambah User</button>
                <table id="data_user" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Username</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Image</th>
                    <th>Level</th>
                    <th>Password</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_user as $value): ?>
                  <tr align="center">
                    <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->username ?></td>
                    <td><?php echo $value->nama ?></td>
                    <td><?php echo $value->phone ?></td>                    
                    <td><img src="<?= base_url('uploads/foto/' . $value->foto) ?>" class="img-thumbnail center-block" border="1" width="20%"></a></td>
                    <td><?php echo $value->level ?></td>
                    <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-reset<?=$value->id ?>" >Reset</button></td>
                    <td>                    
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?=$value->id ?>" ><i class="fa fa-edit"></i></button>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('user/delete/'.$value->id) ?>"><i class='fa fa-trash'></i></a>
                     </td>                
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

<div class="modal fade" id="modal-tambah">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h4 class="modal-title">Tambah User</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form style="padding: 15px;" action="<?= site_url("user/create_action") ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">          
						<div class="row">
							<label for="varchar">Nama</label>
							<input type="text" class="form-control" name="nama" id="nama" value="<?= $nama ?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Username</label>
							<input type="text" class="form-control" name="username" id="username" value="<?= $username ?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Password</label>
							<input readonly type="text" class="form-control" name="password" id="password" value="123"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Level</label>
							<div class="form-check">
                          <input class="form-check-input" type="radio" name="level" value="1" >
                          <label class="form-check-label">1</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="level" value="2" checked>
                          <label class="form-check-label">2</label>
                        </div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
                        
              
							<button type="submit" class="btn btn-primary btn-block "><i class="fas fa-plus"></i> Tambah</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>        

        <?php  $no = 0;
    foreach($data_user as $value): $no++; ?>
<div class="modal fade" id="modal-reset<?=$value->id ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h4 class="modal-title"> Reset Password</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form style="padding: 15px;"  method="POST" action="<?= site_url("user/reset") ?>" enctype="multipart/form-data">
				<div class="form-group">          
						<div class="row">
							<label for="varchar"><h3>Apakah Anda Yakin Password Akan Direset? </h3></label>
						</div>
					</div>
        
        <div class="form-group">          
						<div class="row">
              <input type="hidden" class="form-control" name="foto" id="foto" value="<?=$value->foto?>"  required />
						</div>
					</div>

        <div class="form-group">          
						<div class="row">
							<input type="hidden" class="form-control" name="nama" id="nama" value="<?=$value->nama?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<input type="hidden" class="form-control" name="username" id="username" value="<?=$value->username?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<input type="hidden" class="form-control" name="phone" id="phone" value="<?=$value->phone?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
            <input type="hidden" class="form-control" name="level" id="level" value="<?=$value->level?>"  required />
						</div>
					</div> 

          <div class="form-group">          
						<div class="row">
            <input type="hidden" class="form-control" name="password" id="password" value="<?=$value->level?>"  required />
						</div>
					</div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id" value="<?=$value->id?>">
                        <input type="hidden" name="password" value="123">
                        <button type="submit" class="btn btn-success btn-block">Ya, Reset Password </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>
        
<?php  $no = 0;
    foreach($data_user as $value): $no++; ?>
<div class="modal fade" id="modal-edit<?=$value->id ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title fa fa-edit"> Ubah Level User</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form style="padding: 15px;"  method="POST" action="<?= site_url("user/update_action") ?>" enctype="multipart/form-data">
				<div class="form-group">          
						<div class="row">
              <input readonly type="hidden" class="form-control" name="foto" id="foto" value="<?=$value->foto?>"  required />
            <img src="<?= base_url() . '/uploads/foto/' . $value->foto?>" width="100">
						</div>
					</div>

        <div class="form-group">          
						<div class="row">
							<label for="varchar">Nama </label>
							<input readonly type="text" class="form-control" name="nama" id="nama" value="<?=$value->nama?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Username </label>
							<input readonly type="text" class="form-control" name="username" id="username" value="<?=$value->username?>"  required />
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Phone </label>
							<input readonly type="text" class="form-control" name="phone" id="phone" value="<?=$value->phone?>" >
						</div>
					</div>

          <div class="form-group">          
						<div class="row">
							<label for="varchar">Level </label>
							<div class="form-check">
                          <input class="form-check-input" type="radio" name="level" value="1" <?= $value->level == '1'?'checked':''?> >
                          <label class="form-check-label">1</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="level" value="2" <?= $value->level == '2'?'checked':''?> >
                          <label class="form-check-label">2</label>
                        </div>
						</div>
					</div>          

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id" value="<?=$value->id?>">
                        <input type="hidden" name="password" value="<?=$value->password?>">
                        <button type="submit" class="btn btn-warning btn-block"><i class="fas fa-save"></i> Simpan </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>        


<script>
  $(function () {
    $("#data_user").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>

<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data User akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
      
  }
})
});
</script>