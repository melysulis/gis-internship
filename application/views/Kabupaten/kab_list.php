<div class="content-header">
      <div class="container-fluid">
      <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark ion ion-pie-graph"> Data Kabupaten/Kota </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Kabupaten/Kota</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">  
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-tambah"><i class="fa fa-plus"></i> Tambah Kab/Kota</button>
                <table id="data_kab" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama Kab/Kota</th>
                    <th>Nama Provinsi</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_kab as $value): ?>
                  <tr align="center">
                  <td><?php echo $nomor++ ?></td>
                    <td><?php echo $value->nama_kab ?></td>
                    <td><?php echo $value->nama_prop ?></td>
                    <td>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?=$value->id_kabupaten ?>" ><i class="fa fa-edit"></i></button>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('kabupaten/delete/'.$value->id_kabupaten) ?>"><i class='fa fa-trash'></i></a>
                  </td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

  <div class="modal fade" id="modal-tambah" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h4 class="modal-title">Tambah Kab/Kota</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" action="<?= site_url('kabupaten/create_action') ?>" method="POST"  enctype="multipart/form-data">
					<div class="form-group">          
						<div class="row">
							<label for="varchar">Nama Kab/Kota</label>
							<input type="text" class="form-control" name="nama_kab" id="nama_kab" value="<?=$nama_kab?>" required />
						</div>
					</div>

          <div class="form-group" >
          <div class="row">
          <label for="varchar">Nama Provinsi</label>
                        <select class="form-control select2bs4"  name="id_propinsi" required>
						  <option></option>
						  <?php foreach($data_prop as $value):?>
							<option <?=$id_propinsi == $value->id_propinsi?"selected":''?> value="<?= $value->id_propinsi?>"><?= $value->nama_prop?></option>
							<?php endforeach?>						  						  
                        </select>
					  </div>
          </div>
          
					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_kabupaten" value="<?= $id_kabupaten ?>">              
							<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-plus"></i> Tambah</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php  $no = 0;
                  foreach($data_kab as $value): $no++; ?>
<div class="modal fade" id="modal-edit<?=$value->id_kabupaten?>" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-yellow">
				<h4 class="modal-title fa fa-edit"> Ubah Kab/Kota</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" action="<?= site_url('kabupaten/update_action') ?>" method="POST"  enctype="multipart/form-data">
					<div class="form-group">          
						<div class="row">
							<label for="varchar">Nama Kab/Kota</label>
							<input type="text" class="form-control" name="nama_kab" id="nama_kab" value="<?=$value->nama_kab?>" required />
						</div>
					</div>

          <div class="form-group" >
          <div class="row">
          <label for="varchar">Nama Provinsi</label>
                        <select class="form-control select2bs4"  name="id_propinsi" required>
						  <option></option>
						  <?php foreach($data_prop as $prop):?>
							<option <?=$value->id_propinsi == $prop->id_propinsi?"selected":''?> value="<?= $prop->id_propinsi?>"><?= $prop->nama_prop?></option>

							<?php endforeach?>						  						  
                        </select>
					  </div>
          </div>

					<div class="form-group">
						<div class="row">
                        <input type="hidden" name="id_kabupaten" value="<?=$value->id_kabupaten?>">
              
                        <button type="submit" class="btn btn-warning btn-block "><i class="fas fa-save"></i> Simpan </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>
        

<script>
  $(function () {
    $("#data_kab").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>

<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data kab/kota akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
      
  }
})
});
</script>