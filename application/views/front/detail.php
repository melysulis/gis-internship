<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?=$nama_web->option_value?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url()?>frontpage/assets/img/map.png" rel="icon">
  <link href="<?= base_url()?>frontpage/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="<?= base_url()?>frontpage/https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url()?>frontpage/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url()?>frontpage/assets/css/style.css" rel="stylesheet">


  <!-- =======================================================
  * Template Name: TheEvent - v2.3.1
  * Template URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header-fixed">
    <div class="container">
    <div id="logo" class="pull-left">
        <!-- Uncomment below if you prefer to use a text logo -->
        <!-- <h1><a href="#intro">The<span>Event</span></a></h1>-->
        <h3><a href="<?= site_url('frontpage') ?>" class="scrollto"><img src="<?= base_url()?>frontpage/assets/img/map.png" alt="" title=""><?=$nama_web->option_value?></a></h3>
      </div>

      <div id="logo" class="pull-left">
        <!-- Uncomment below if you prefer to use a text logo -->
        <!-- <h1><a href="#intro">The<span>Event</span></a></h1>-->
        <a href="index.html" class="scrollto"><img href="<?= base_url()?>frontpage/assets/img/logo.png" alt="" title=""></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?= site_url('frontpage') ?>">Beranda</a></li>
          <li><a href="<?= site_url('frontpage')?>#about">Tentang</a></li>
          <li><a href="<?= site_url('frontpage')?>#schedule">Top 10</a></li>
          <li><a href="<?= site_url('frontpage')?>#venue">Peta</a></li>
          <li><a href="<?= site_url('frontpage')?>#buy-tickets">Data PKL</a></li>
          <li><a href="<?= site_url('frontpage')?>#supporters">Mitra</a></li>
          <li><a href="<?= site_url('frontpage')?>#contact">Hubungi Kami</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->

  <div class="content-wrapper">

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <?= $contents?>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

 
  <?php $this->load->view('front/footer')?>

  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?= base_url()?>frontpage/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/superfish/superfish.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/hoverIntent/hoverIntent.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url()?>frontpage/assets/js/main.js"></script>

</body>

</html>