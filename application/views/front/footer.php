<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-md-6 footer-info">
          <h3><img src="<?= base_url()?>frontpage/assets/img/map.png" alt="" title=""><?=$nama_web->option_value?></a></h3>
            <p><?=$footer->option_value?></p>
          </div>

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Link Terkait</h4>
            <ul>
            <?php 
                  foreach($data_link as $value): ?>
            <li><i class="fa fa-angle-right"></i> <a href="<?= $value->url ?>"><td><?php echo $value->nama_link ?></td> </a></li>
              <?php endforeach ?>
          </div>

         
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>SIGPKL</strong>. 2021
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=TheEvent
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->