<main id="main" class="main-page">

    <!-- ======= Speaker Details Sectionn ======= -->
    <section id="speakers-details">
      <div class="container">
        <div class="section-header">
          <h2>Data Lokasi PKL</h2>
          <p></p>
        </div>
        <div class="row">
          <div class="col-md-12">
        

        <div class="form">
          <form action="<?=site_url('frontpage/detail_lokasi')?>" method="post">
            <div class="form-row">
            <div class="form-group col-md-4" >            
            <select class="form-control select2bs4" name="keyword" >
						  <option></option>
						  <?php foreach($view_all as $value):?>
							<option value="<?= $value->nama_instansi?>"><?= $value->nama_instansi?></option>
							<?php endforeach?>						  						  
                        </select>
            </div>
              
              <div class="form-group col-md-2">
              <button type="submit">Cari</button>
              </div>
            </div>
          </form>
        </div>
          
          <table id="view_1" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama Instansi</th>
                    <th>Jenis</th>
                    <th>Alamat</th>
                    <th>Kab/Kota</th>
                    <th>Provinsi</th>
                    <th>Jumlah Mahasiswa</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  if('keyword' == ''){
                    $nomor = 1;
                  foreach($view_all as $value): ?>                 
                  <tr align="center">                  
                  <td><?php echo $nomor++ ?></td>
                    <td><a href="<?=$value->website?>"><?php echo $value->nama_instansi ?></a></td>
                    <td><?php echo $value->jenis ?></td>
                    <td><?php echo $value->alamat ?></td>
                    <td><?php echo $value->nama_kab ?></td>
                    <td><?php echo $value->nama_prop ?></td>                    
                    <td><?php echo $value->jml_mhs ?></td>
                  </tr>
                  <?php endforeach ?>
                 <?php }else{          ?>     
                  <?php  $nomor = 1;
                foreach($view_1 as $row): ?>
                <tr align="center">                  
                  <td><?php echo $nomor++ ?></td>
                    <td><a href="<?=$row->website?>"><?php echo $row->nama_instansi ?></a></td>
                    <td><?php echo $row->jenis ?></td>
                    <td><?php echo $row->alamat ?></td>
                    <td><?php echo $row->nama_kab ?></td>
                    <td><?php echo $row->nama_prop ?></td>                    
                    <td><?php echo $row->jml_mhs ?></td>
                  </tr>
                <?php endforeach ?>
                <?php } ?>

                  </tbody>
                  
                </table>
          </div>         

        </div>
      </div>

    </section>

  </main><!-- End #main -->
  <script>
  $(function () {
    $("#view_1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>