<main id="main" class="main-page">

    <!-- ======= Speaker Details Sectionn ======= -->
    <section id="speakers-details">
      <div class="container">
        <div class="section-header">
          <h2>Data Mahasiswa PKL</h2>
          <p></p>
        </div>

        <div class="row">
          <div class="col-md-12">
          <div class="form">
          <form action="<?=site_url('frontpage/detail_mhs')?>" method="post">
            <div class="form-row">
              <div class="form-group col-md-4">
                <input type="text" name="keyword" class="form-control" id="name" placeholder="Cari Nama Mahasiswa"  />
              </div>
              <div class="form-group col-md-8">
              <button type="submit">Cari</button>
              </div>
            </div>
          </form>
        </div>
          <table id="data_mhs" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Angkatan</th>
                    <th>Lokasi PKL</th>
                    <th>Tgl Mulai</th>
                    <th>Tgl Selesai</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  $nomor = 1;
                  foreach($data_mhs as $value): ?>
                  <tr align="center">
                  <td><?php echo $nomor++ ?></td>
                  <td><?php echo $value->nama ?></td>
                  <td><?php echo $value->angkatan ?></td>
                    <td><?php echo $value->nama_instansi ?></a></td>
                    <td><?php echo $value->tgl_mulaiPKL ?></td>
                    <td><?php echo $value->tgl_selesaiPKL ?></td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
          </div>

          

        </div>
      </div>

    </section>

  </main><!-- End #main -->