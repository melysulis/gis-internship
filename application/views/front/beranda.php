<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?=$nama_web->option_value?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url()?>frontpage/assets/img/map.png" rel="icon">
  <link href="<?= base_url()?>frontpage/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url()?>frontpage/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url()?>frontpage/assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url()?>frontpage/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: TheEvent - v2.3.1
  * Template URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <?= $map['js'];?>
<script>
function setToForm(latitude,longitude)
{
	$('input[name="latitude"]').val(latitude);
	$('input[name="longitude"]').val(longitude);
}
</script>
</head>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
<script src="<?= base_url()?>leaflet-search/src/leaflet-search.js"></script>
<link rel="stylesheet" href="<?= base_url()?>leaflet-search/src/leaflet-search.css" />

<link rel="stylesheet" href="<?= base_url()?>leaflet-search/src/Control-Geocoder.css" />
 <script src="<?= base_url()?>leaflet-search/src/Control-Geocoder.js"></script> 
 <link rel="stylesheet" href="<?= base_url()?>leaflet-search/src/leaflet-panel-layers.css" />
 <script src="<?= base_url()?>leaflet-search/src/leaflet-panel-layers.js"></script> 

 
 <link rel="stylesheet" href="<?= base_url()?>leaflet-panel/leaflet-panel-layers.min.css" />
 <script src="<?= base_url()?>leaflet-panel/leaflet-panel-layers.min"></script> 
 
 <link rel="stylesheet" href="<?= base_url()?>leaflet-panel/leaflet-panel-layers.src.css" />
 <script src="<?= base_url()?>leaflet-panel/leaflet-panel-layers.src.js"></script> 
<body>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <!-- Uncomment below if you prefer to use a text logo -->
        <!-- <h1><a href="#intro">The<span>Event</span></a></h1>-->
        <h3><a href="<?= site_url('frontpage') ?>" class="scrollto"><img src="<?= base_url()?>frontpage/assets/img/map.png" alt="" title=""><?=$nama_web->option_value?></a></h3>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?= site_url('frontpage')?>">Beranda</a></li>
          <li><a href="#about">Tentang</a></li>
          <li><a href="#schedule">Top 10</a></li>
          <li><a href="#venue">Peta</a></li>
          <li><a href="#buy-tickets">Data PKL</a></li>
          <li><a href="#supporters">Mitra</a></li>
          <li><a href="#contact">Hubungi Kami</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Intro Section ======= -->
  <section id="intro">
    <div class="intro-container" data-aos="zoom-in" data-aos-delay="100">
      <h1 class="mb-4 pb-0"><?= $title->option_name ?><br><span><?= $title->option_value ?></span> </h1>
      <p class="mb-4 pb-0"><?= $caption->option_value?></p>
      <a href="#venue" class="about-btn scrollto"> Lihat Peta</a>
    </div>
  </section><!-- End Intro Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-10">
            <h2><?= $tentang->option_name ?></h2>
            <p><?= $tentang->option_value ?></p>
          </div>
          <div class="col-lg-3">
            <h3></h3>
            <p></p>
          </div>
          <div class="col-lg-3">
            <h3></h3>
            <p></p>
          </div>
        </div>
      </div>
    </section><!-- End About Section -->
  

    <!-- ======= Schedule Section ======= -->
    <section id="schedule" class="section-with-bg">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h2>TOP 10</h2>
          <p></p>
        </div>

        <h3 class="sub-heading">Berikut daftar 10 teratas instansi dengan jumlah mahasiswa PKL terbanyak.</h3>

        <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

          <!-- Schdule Day 1 -->
          <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
          <?php 
                  $nomor = 1;
                  foreach($data_top10 as $value): ?>
            <div class="row schedule-item">
              <div class="col-md-2"><time><?php echo $nomor++ ?></time></div>
              <div class="col-md-10">
                <div class="speaker">
                <a href="<?= $value->website ?>"><img src="<?= base_url('uploads/' . $value->icon) ?>" class="img-fluid" alt=""></a>
                </div>
                <a href="<?= $value->website ?>"><h4><?php echo $value->nama_instansi ?> <span></span></h4></a>
                <p><?=$value->jml_mhs?> Mahasiswa PKL </p>
              </div>
            </div>
            <?php endforeach ?>            
          </div>
          <!-- End Schdule Day 1 -->
        </div>
      </div>
    </section><!-- End Schedule Section -->

    <!-- ======= Venue Section ======= -->
    <section id="venue">

      <div class="container-fluid" data-aos="fade-up">

        <div class="section-header">
          <h2>PETA</h2>
          <p>Cari lokasi berdasarkan provinsi, kabupaten/kota, atau kecamatan</p>
        </div>
        <div class="row">
<?php 
                  foreach($data_jenis as $value): ?>
          <div class="col-lg-2" data-aos="fade-up" data-aos-delay="100">
            <div class="card mb- mb-lg-0">
              <div class="card-body">
                <h6 class="text-center"><?php echo $value->jenis ?></h6>
                <h6 class="card-price text-center"><img src="<?= base_url() . '/uploads/marker_jenis/' . $value->marker_jenis?>" width="20%"></h6>    
                      
              </div>
            </div>
          </div>        
          <?php endforeach ?>            
        </div><p></p>
        <div id="mapid" style= "width: 1320px; height: 500px;"></div>
        <script>

var mymap = L.map('mapid').setView([-2.3095866886253678, 113.70759472281762], 5);
      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      }).addTo(mymap);
      
         <?php foreach($data_lokasi as $value):?>
      var icon = L.icon({
      iconUrl: '<?= base_url('uploads/marker_jenis/' . $value->marker_jenis)?>',
      iconSize: [30, 35],
      iconAnchor: [22, 65],
      popupAnchor: [-3, -55]
      });
      
      L.marker([<?=$value->latitude?>, <?=$value->longitude?>], {icon:icon})
      .bindPopup("<h4><a href=<?= $value->website?> ><p><?=$value->nama_instansi?></p></a></h4><img src=<?= base_url('uploads/' . $value->icon) ?> width=60%><p>Alamat : <?=$value->alamat?></p><p><?=$value->nama_kab?>, <?=$value->nama_prop?> </p><p>No Tlp : <?=$value->no_tlp?></p><p>Email : <?=$value->email?></p>")
      .addTo(mymap);
      <?php endforeach?>        
      L.Control.geocoder().addTo(mymap);
      mymap.addControl( new L.Control.PanelLayers() );
      
</script>  
<p></p>  

    </section><!-- End Venue Section -->

    <!-- ======= Buy Ticket Section ======= -->
    <section id="buy-tickets" class="section-with-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2>DATA PKL</h2>
          <p>Berikut jumlah data mahasiswa dan instansi</p>
        </div>

        <div class="row">
          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <div class="card mb-5 mb-lg-0">
              <div class="card-body">
                <h5 class="card-title text-muted text-uppercase text-center">Jumlah Mahasiswa PKL</h5>
                <h6 class="card-price text-center"><?= $count_mhs?></h6>    
                <hr>
                <a href="<?= site_url('frontpage/detail_mhs') ?>"><h6 class="text-center">Selengkapnya</a></h6>        
              </div>
            </div>
          </div>
          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="200">
            <div class="card mb-5 mb-lg-0">
              <div class="card-body">
                <h5 class="card-title text-muted text-uppercase text-center">Jumlah Instansi</h5>
                <h6 class="card-price text-center"><?= $count_lokasi?></h6>
                <hr>
                <a href="<?= site_url('frontpage/detail_lokasi') ?>"><h6 class="text-center">Selengkapnya</a></h6>
              </div>
            </div>
          </div>
          <!-- Pro Tier -->
          
        </div>

      </div>


    </section><!-- End Buy Ticket Section -->

    
    <!-- ======= Supporters Section ======= -->
    <section id="supporters" class="section-with-bg">

      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h2>Mitra</h2>
        </div>

        <div class="owl-carousel gallery-carousel" data-aos="fade-up" data-aos-delay="50">
        <?php
						foreach ($data_mitra as $value) :
						?>
          
            <div class="supporter-logo">
            
          <a href="<?= $value->website ?>"><img src="<?= base_url('uploads/' . $value->icon) ?>" class="img-fluid" alt=""></a>
            
          </div>
          <?php endforeach; ?>
        </div>

      </div>

    </section><!-- End Sponsors Section -->
  
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="section-bg">

      <div class="container" data-aos="fade-up">

        <div class="section-header">
          <h2>Hubungi Kami</h2>
          <p>Sudah menemukan lokasi PKL? </p>
          <p>Ajukan permohonan surat pengantar PKL pada link di bawah ini! </p>
        </div>
        
        <div class="row contact-info">
          <div class="col-md-12">
          <div class="text-center"><a href="<?=$link_surat->url?>"><?=$link_surat->nama_link?></a></div>            
          </div>

        </div>        

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  

  <?php $this->load->view('front/footer')?>

  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?= base_url()?>frontpage/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/php-email-form/validate.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/superfish/superfish.min.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/hoverIntent/hoverIntent.js"></script>
  <script src="<?= base_url()?>frontpage/assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url()?>frontpage/assets/js/main.js"></script>

</body>

</html>