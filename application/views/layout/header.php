<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

   
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">    

    <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
            
            <i class="fa fa-user fa-fw"></i> Hallo, <?= $nama_user?> <b class="caret"></b></a>
            
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="<?= site_url('user/my_profile') ?>" class="dropdown-item "><i class="fa fa-user fa-fw"></i> Profil Saya </a></li>
              <li><a href="<?= site_url('user/edit_profile') ?>" class="dropdown-item"><i class="nav-icon fas fa-edit"></i> Ubah Profil</a></li>

              <li class="dropdown-divider"></li>
              <!-- Level two dropdown-->
              <li><a href="<?= site_url('frontpage') ?>" class="dropdown-item "><i class="fas fa-globe"></i> Halaman Depan </a></li>
              <li><a href="<?= site_url('auth/logout_user') ?>" class="dropdown-item logout"><i class="fa fa-sign-out-alt fa-fw"></i> Keluar </a></li>
              
              <!-- End Level two -->
            </ul>
          </li>
   
    </ul>
    
  </nav>
  <!-- /.navbar -->
  
  <script>
$('.logout').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin akan keluar?',
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes'
}).then((result) => {
    if (result.value){
    document.location.href = href;
          
  }
})
});
</script>