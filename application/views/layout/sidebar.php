

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      
    <span class="brand-text font-weight-light" >Sistem Informasi Geografis</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="<?= base_url('uploads/foto/' . $foto) ?>" class="img-circle elevation-2" alt="user image" >
        </div>
        <div class="info" >
          
          <a href="<?= site_url('user/my_profile') ?>" class="d-block"><?=$nama_user ?>    </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
      
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
        <a href="<?= site_url('dashboard') ?>" class="nav-link <?= $this->uri->segment(1) == 'dashboard' ? 'active' : '' ?>">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>Dahsboard</p>
					</a>
				</li>
        <li class="nav-header">KELOLA DATA PKL</li>
				<li class="nav-item">
        <a href="<?= site_url('mahasiswa') ?>" class="nav-link <?= $this->uri->segment(1) == 'mahasiswa' ? 'active' : '' ?>">
						<i class="nav-icon fas fa-users"></i>
						<p>Mahasiswa</p>
					</a>
				</li>

					<li class="nav-item">
          <a href="" class="nav-link 'lokasi' ? 'active' : '' ?>">
							<i class="nav-icon fas fa-map-marker-alt"></i>
              <i class="right fas fa-angle-left"></i>
							<p>Lokasi</p>
						</a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?= site_url('lokasi') ?>" class="nav-link <?= $this->uri->segment(1) == 'lokasi' ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Instansi</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?= site_url('jenis_lokasi') ?>" class="nav-link <?= $this->uri->segment(1) == 'jenis_lokasi' ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jenis</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?= site_url('propinsi') ?>" class="nav-link <?= $this->uri->segment(1) == 'propinsi' ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Provinsi</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?= site_url('kabupaten') ?>" class="nav-link <?= $this->uri->segment(1) == 'kabupaten' ? 'active' : '' ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kab/Kota</p>
                </a>
              </li>
            </ul>
              
            <li class="nav-header">KELOLA HALAMAN DEPAN</li>
            <li class="nav-item">
        <a href="<?= site_url('general') ?>" class="nav-link <?= $this->uri->segment(1) == 'general' ? 'active' : '' ?>">
						<i class="fas fa-globe nav-icon"></i>
						<p>General</p>
					</a>
				</li>
            <li class="nav-item">
        <a href="<?= site_url('tentang') ?>" class="nav-link <?= $this->uri->segment(1) == 'tentang' ? 'active' : '' ?>">
						<i class="fas fa-cog nav-icon"></i>
						<p>Tentang</p>
					</a>
				</li>        
            <li class="nav-item">
        <a href="<?= site_url('mitra') ?>" class="nav-link <?= $this->uri->segment(1) == 'mitra' ? 'active' : '' ?>">
						<i class="fas fa-handshake nav-icon"></i>
						<p>Mitra</p>
					</a>
				</li>
        <li class="nav-item">
        <a href="<?= site_url('links') ?>" class="nav-link <?= $this->uri->segment(1) == 'links' ? 'active' : '' ?>">
						<i class="fas fa-link nav-icon"></i>
						<p>Links</p>
					</a>
				</li>
        <?php if ($this->session->userdata('level') == 1) { ?>
        <li class="nav-header">KELOLA DATA USER</li>
				<li class="nav-item">
        <a href="<?= site_url('user/index') ?>" class="nav-link <?= $this->uri->segment(1) == 'user' ? 'active' : '' ?>">
						<i class="fas fa-users nav-icon"></i>
						<p>Users</p>
					</a>
				</li>
        <?php } ?>        
            </ul>

          									
					
		
		</nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>