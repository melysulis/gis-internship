<div class="content-header">
	<div class="container-fluid">
  <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-tachometer-alt"> Dashboard </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>    

      <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?= $count_mhs?></h3>
                <p>Data Mahasiswa PKL</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-users"></i>
              </div>
              <a href="<?= site_url('mahasiswa') ?>" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3><?= $count_lokasi?></h3>

                <p>Data Lokasi PKL</p>
              </div>
              <div class="icon">
                <i class="nav-icon fas fa-map-marker-alt"></i>
              </div>
              <a href="<?= site_url('lokasi') ?>" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <h3><?= $count_prop?></h3>

                <p>Provinsi</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="<?= site_url('propinsi') ?>" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3><?= $count_kab?></h3>

                <p>Kabupaten/Kota</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="<?= site_url('kabupaten') ?>" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div> 

        <div class="row">
        <div class="col-md-12">
          <div class="card card-info">
              <div class="card-header">
                <i class="fas fa-map-marked-alt"></i> Peta Lokasi PKL</i>  
                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                  </button></div>                                
              </div>
              <div class="card-body">                
              <div class="row">
<?php 
                  foreach($data_jenis as $value): ?>
          <div class="col-lg-2" >
            <div class="card mb- mb-lg-0">
              <div class="card-body">
                <h6 class="text-center"><?php echo $value->jenis ?></h6>
                <h6 class="text-center"><img src="<?= base_url() . '/uploads/marker_jenis/' . $value->marker_jenis?>" width="20%"></h6>    
                      
              </div>
            </div>
          </div>        
          <?php endforeach ?>            
        </div>
        <p></p>  
        <div id="mapid" style= "width: 1000px; height: 500px;"></div>
          
<script>

  var mymap = L.map('mapid').setView([-2.3095866886253678, 113.70759472281762], 5);
      Layer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        }).addTo(mymap);

        <?php foreach($data_lokasi as $value):?>
        var icon = L.icon({
	      iconUrl: '<?= base_url('uploads/marker_jenis/' . $value->marker_jenis)?>',
	      iconSize: [30, 35],
	      iconAnchor: [22, 65],
	      popupAnchor: [-3, -55]
        });
        
        L.marker([<?=$value->latitude?>, <?=$value->longitude?>], {icon:icon})
        .bindPopup("<h4><a href=<?= $value->website?> ><p><?=$value->nama_instansi?></p></a></h4><img src=<?= base_url('uploads/' . $value->icon) ?> width=60%><p>Alamat : <?=$value->alamat?></p><p><?=$value->nama_kab?>, <?=$value->nama_prop?> </p><p>No Tlp : <?=$value->no_tlp?></p><p>Email : <?=$value->email?></p>")
        .addTo(mymap);
        <?php endforeach?>
        L.Control.geocoder().addTo(mymap); 

var panelLayers = new L.Control.PanelLayers(overLayers  
);
mymap.addControl(panelLayers);
</script>    
        
        
               