<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark "> Form Data Mahasiswa </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item "><a href="<?= site_url("mahasiswa")?>">Data Mahasiswa</a></li>
              <li class="breadcrumb-item active">Form Data Mahasiswa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>
<div class="card card-info">
              <div class="card-header">
                
                <h3 class="card-title">Form Data Mahasiswa</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="POST" action="<?=$action?>">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nim" name="nim" value="<?=$nim?>" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama" name="nama" value="<?=$nama_mhs?>" required/ >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="form-check">
                          <input class="form-check-input" type="radio" name="jenis_kelamin" value="L" <?= $jenis_kelamin == 'L'?'checked':''?>>
                          <label class="form-check-label">L</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="jenis_kelamin" value="P" <?= $jenis_kelamin == 'P'?'checked':''?>>
                          <label class="form-check-label">P</label>
                        </div>
                    </div>
                    <div class="form-group row">
                    <label for="telepon" class="col-sm-2 col-form-label">Angakatan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="angkatan" name="angkatan" value="<?=$angkatan?>" placeholder="Contoh : 2017">
                    </div>
                  </div>
                  
                  <div class="form-group row" >
                        <label for="alamat" class="col-sm-2 col-form-label">Nama Instansi</label>
                        <div class="col-sm-10">
                        <select class="form-control select2bs4"  name="id_lokasi" required>
						  <option></option>
						  <?php foreach($data_lokasi as $value):?>
							<option <?=$id_lokasi == $value->id_lokasi?"selected":''?> value="<?= $value->id_lokasi?>"><?= $value->nama_instansi?></option>

							<?php endforeach?>						  						  
                        </select>
                        </div>
					  </div>
                      <div class="form-group row">
                    <label for="tanggal" class="col-sm-2 col-form-label">Tanggal Mulai PKL</label>                  
                    <div class="col-sm-10">  
                      <input type="date" class="form-control" id="tgl_mulaiPKL" name="tgl_mulaiPKL" value="<?=$tgl_mulaiPKL?>" >                    
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="tanggal" class="col-sm-2 col-form-label">Tanggal Selesai PKL</label>                    
                    <div class="col-sm-10">
                      <input type="date" class="form-control" id="tgl_selesaiPKL" name="tgl_selesaiPKL" value="<?=$tgl_selesaiPKL?>" >   
                      </div>                 
                  </div>          

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <input type="hidden" name="id_mhs" value="<?=$id_mhs?>">
                  <button type="submit" class="btn btn-info"><?=$button?></button>
                  <button class="btn btn-default float-right"><a href="<?= site_url('mahasiswa') ?>">Kembali</a></button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>