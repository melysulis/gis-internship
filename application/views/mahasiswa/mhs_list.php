<div class="content-header">
      <div class="container-fluid">
      <?= $this->session->flashdata('message');?>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark nav-icon fas fa-users"> Data Mahasiswa </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= site_url("Dashboard")?>">Dashboard</a></li>
              <li class="breadcrumb-item active">Data Mahasiswa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      </div>

      <div class="row">
          <div class="col-12">
            
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">                
              <a class="mb-3 btn btn-primary" href="<?= site_url('mahasiswa/create') ?>"><i class="fa fa-plus"></i> Tambah Mahasiswa</a>
                <table id="data_mhs" class="table table-bordered table-striped">
                  <thead>
                  <tr align="center">
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Lokasi PKL</th>
                    <th>TGL Mulai PKL</th>
                    <th>TGL Selesai PKL</th>
                    <th>Aksi</th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  <?php 
                  foreach($data_mhs as $value): ?>
                  <tr align="center">
                    <td><?php echo $value->nim ?></td>
                    <td><?php echo $value->nama ?></td>
                    <td><?php echo $value->nama_instansi ?></td>
                    <td><?php echo $value->tgl_mulaiPKL ?></td>
                    <td><?php echo $value->tgl_selesaiPKL ?></td>
                    <td>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-detail<?=$value->id_mhs ?>" ><i class="fa fa-eye"></i></button>
                    <a class="mb-3 btn btn-warning" href="<?= site_url('mahasiswa/update/'.$value->id_mhs) ?>"><i class='fa fa-edit'></i></a>
                    <a class="mb-3 btn btn-danger hapus" href="<?= site_url('mahasiswa/delete/'.$value->id_mhs) ?>" ><i class='fa fa-trash'></i></a>
                  </td>
                  </tr>
                  <?php endforeach ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>

        <?php  $no = 1;
                  foreach($data_mhs as $row): $no++; ?>
<div class="modal fade" id="modal-detail<?=$row->id_mhs?>" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<h4 class="modal-title fa fa-eye"> Detail Mahasiswa</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form style="padding: 15px;" method="POST" action="" >
					<div class="form-group">
          
          <table class="table table-striped">
					<input type="hidden" name="id_mhs" value="<?=$row->id_mhs?>">
						<tr>
							<td width="20%"><b>NIM</b></td>
							<td><?= $row->nim ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nama</b></td>
							<td><?= $row->nama ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Jenis Kelamin</b></td>
							<td><?= $row->jenis_kelamin == "L"?"Laki-Laki":"Perempuan" ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Angkatan</b></td>
							<td><?= $row->angkatan ?></td>
						</tr>            
            <tr>
							<td width="20%"><b>Lokasi PKL</b></td>
							<td><?= $row->nama_instansi ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Tanggal Mulai PKL</b></td>
							<td><?= $row->tgl_mulaiPKL ?></td>
						</tr>
            <tr>
							<td width="20%"><b>Tanggal Selesai PKL</b></td>
							<td><?= $row->tgl_selesaiPKL ?></td>
						</tr>
					</table>
					<a href="<?= site_url('mahasiswa/update/' . $row->id_mhs) ?>" class="btn btn-warning">
						<i class="fa fa-edit"></i> Ubah 
					</a>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endforeach ?>
        

<script>
  $(function () {
    $("#data_mhs").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    
     
  });
</script>

<script>
$('.hapus').on('click', function (e) {

e.preventDefault();
const href = $(this).attr('href')

Swal.fire({
    title : 'Apakah anda yakin?',
    text : "Data mahasiswa akan dihapus",
    type :'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Hapus data!'
}).then((result) => {
    if (result.value){
    document.location.href = href;
    Swal.fire(
      'Hapus!',
      'Data Berhasil Dihapus.',
      'success')
      
  }
})
});
</script>