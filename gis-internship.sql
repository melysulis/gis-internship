-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2021 at 12:35 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gis-internship`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenis_lokasi`
--

CREATE TABLE `jenis_lokasi` (
  `id_jenis` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `marker_jenis` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_lokasi`
--

INSERT INTO `jenis_lokasi` (`id_jenis`, `jenis`, `marker_jenis`) VALUES
(1, 'Perseroan Komanditer (CV)', 'hijau.png'),
(2, 'Peseroan Terbatas (PT)', 'merah.png'),
(3, 'Stasiun Televisi', 'marker_1628387089.png'),
(6, 'Sekolah', 'marker_1628387161.png'),
(7, 'Pemerintahan', 'marker_1628521500.png');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id_kabupaten` int(11) NOT NULL,
  `nama_kab` varchar(50) NOT NULL,
  `id_propinsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`id_kabupaten`, `nama_kab`, `id_propinsi`) VALUES
(1, 'Purwokerto', 1),
(2, 'Bandung', 2),
(4, 'Semarang', 1),
(5, 'Mojokerto', 3),
(6, 'Banyumas', 1),
(7, 'Brebes', 1),
(8, 'Kuningan', 2),
(9, 'Pemalang', 1),
(10, 'Bantul', 5),
(11, 'Kebumen', 1),
(12, 'Cilacap', 1),
(13, 'Baubau', 10),
(14, 'Makassar', 9),
(15, 'Cirebon', 2),
(16, 'Sumedang', 2),
(17, 'Bogor', 2),
(18, 'Jakarta Pusat', 4),
(19, 'Bekasi', 2),
(20, 'Medan', 7),
(21, 'Sleman', 5),
(22, 'Garut', 2),
(23, 'Wonosobo', 1),
(24, 'Jakarta Selatan', 4),
(25, 'Deli Serdang', 7),
(26, 'Banjar Baru', 8),
(27, 'Serang', 6);

-- --------------------------------------------------------

--
-- Table structure for table `koordinat`
--

CREATE TABLE `koordinat` (
  `id_koordinat` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `id_lokasi` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `koordinat`
--

INSERT INTO `koordinat` (`id_koordinat`, `latitude`, `longitude`, `id_lokasi`, `created_at`) VALUES
(1, -7.43469884807456, 109.24929031060782, 2, '2021-06-17 03:32:49'),
(2, 0, 0, 0, '2021-06-20 04:03:49'),
(3, 0, 0, 0, '2021-06-22 01:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id_link` int(11) NOT NULL,
  `nama_link` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id_link`, `nama_link`, `url`) VALUES
(1, 'PKL Informatika', 'https://pkl.informatika.app/'),
(2, 'S1 Teknik Informatika', 'https://s1if.ittelkom-pwt.ac.id/'),
(3, 'Permohonan Surat Pengantar PKL', 'http://suket.ittelkom-pwt.ac.id/index.php/form/suket_pkl');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `nama_instansi` varchar(500) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(500) NOT NULL,
  `id_kabupaten` int(25) NOT NULL,
  `id_propinsi` int(25) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `icon` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_instansi`, `id_jenis`, `alamat`, `no_tlp`, `email`, `website`, `id_kabupaten`, `id_propinsi`, `latitude`, `longitude`, `icon`, `created_at`) VALUES
(1, 'Jenderal Corp', 1, 'Jalan Menteri Supeno, Perum Griya Permata Residence No B9 Kecamatan Sokaraja ', '085742798737', 'jenderalcorp@gmail.com', 'https://jenderalcorp.com/', 6, 1, -7.457807737700499, 109.28090092554484, 'icon_1628326604.jpg', '2021-08-21 01:30:04'),
(3, 'Badan Pusat Statistik Banyumas', 7, 'Jl. Warga Bhakti No. 5 Purwokerto 53114', '0281 635496', ' bps3302@bps.go.id', 'https://banyumaskab.bps.go.id/', 1, 1, -7.4188281727758145, 109.2478659241027, 'icon_1627897075.PNG', '2021-08-07 14:21:33'),
(4, 'Banyumas TV', 3, 'Jl. HR Bunyamin No. 106 Pabuaran, Purwokerto', '0281-6577020', 'bms.teve@yahoo.com', 'http://www.banyumastv.com', 1, 1, -7.391557345353098, 109.2474950118876, 'icon_1627897085.png', '2021-08-02 09:38:05'),
(5, 'Mitranet Software Online', 2, 'Jalan Gerilya Tengah, Komplek Ruko GKI Blok B N0.4-5 Purwokerto', '0281622 789', '', 'http://microbanking-online.com/', 1, 1, -7.441186040483019, 109.23762142554456, 'icon_1627897096.jpg', '2021-08-02 09:38:16'),
(6, 'Satelit TV Purwokerto', 3, 'Jl. Dr. Angka, Glempang, Bancarkembar, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53114', '+622817771242', '', '', 1, 1, -7.416279291169823, 109.24286331020436, 'icon_1627897108.jpg', '2021-08-07 14:20:56'),
(7, 'CAZH TEKNOLOGI INOVASI', 2, 'Jl. Waru IV Perumahan Graha Tanjung Elok No.25, Bojong, Tanjung, Kec. Purwokerto Selatan', '085526000647', 'hello@cazh.id', 'https://cazh.id', 1, 1, -7.436769628111732, 109.23349991533993, 'icon_1629163827.png', '2021-08-17 01:30:27'),
(8, 'Rumah Mesin Yogyakarta', 1, 'Jl. Parangtritis No.Km 5,6, Tarudan, Bangunharjo, Kec. Sewon', '081222229224', '', 'https://www.rumahmesin.com/', 10, 5, -7.84569695648022, 110.36079762595028, 'icon_1628325073.png', '2021-08-07 08:31:13'),
(9, 'Acak Solusi (ODISYS)', 2, 'Ruko Grand Galaxy City, Blok RSN-2 No. 66, RT.001/RW.002, Jaka Setia, Bekasi Selatan', '+62 21 8946 522', 'marketing@odisys.id', 'https://odisys.id/', 19, 2, -6.265910194865438, 106.9708286255328, 'icon_1628325202.jpg', '2021-08-07 08:33:22'),
(10, 'Hagatekno Mediata', 2, 'l. Bunga Cemp. X, Padang Bulan Selayang II, Kec. Medan Selayang ', '081260000090', '', 'http://hagatekno.com/', 20, 7, 3.5546220873050953, 98.63909603493843, 'icon_1628325218.PNG', '2021-08-07 08:33:38'),
(11, 'HEPTACO Digital Media', 2, 'Jl. Garuda, Cupuwatu I, Purwomartani, Kec. Kalasan', '+6285799995565', '', 'https://hepta.co.id/', 21, 5, -7.774968669520972, 110.4520521660333, 'icon_1628325243.jpg', '2021-08-07 08:34:03'),
(12, 'PT. Icon+ perwakilan Kalimantan Selatan', 2, 'Jln.Roulin, Guntung Payung, Kec. Landasan Ulin', '05115914070', '', '', 26, 8, -3.3368333697675645, 114.8033720202996, 'icon_1628325262.png', '2021-08-07 08:34:22'),
(13, 'PT. Inacom.id', 2, 'Jl. Wijaya Kusuma Raya No.36, RT.02/RW.14, Cilendek Bar., Kec. Bogor Barat.', '081514886388', '', '', 17, 2, -6.5685569646524655, 106.76931129140536, 'icon_1628325295.png', '2021-08-07 08:34:55'),
(14, 'PT. Indonesia Comnet Plus (icon+)', 2, 'Jl. Kh. Abdul Rochim No.1, RT.9/RW.1, Kuningan Bar., Kec. Mampang Prpt.', '0215253019', '', '', 24, 4, -6.232795674874204, 106.82181248355833, 'icon_1628325317.png', '2021-08-07 08:35:17'),
(15, 'Kreasi Kode', 2, 'Jl. Garuda No.78A, Manukan, Condong Catur, Condongcatur, Sleman, Manukan, Condongcatur, Kec. Depok', '08122614411', 'INFO@KODE.CO.ID', 'http://www.kode.co.id/', 21, 5, -7.745238757091159, 110.39862199140539, 'icon_1628325332.jpeg', '2021-08-07 08:35:32'),
(16, 'PT. Layana Computindo', 2, 'Jl. Bima No.164B, Wonorejo, Sariharjo, Kec. Ngaglik', '081804251557', 'marketing@layana.id', 'http://layana.id/', 21, 5, -7.7378195705755095, 110.38013625078632, 'icon_1628325352.png', '2021-08-07 08:35:52'),
(17, 'PT. Media Sarana Data', 2, 'Area Sawah, Nogotirto, Kec. Gamping', '0811 274345', 'info@gmedia.co.id', 'http://www.gmedia.net.id/', 21, 5, -6.980143407091028, 110.42288171536964, 'icon_1628325367.png', '2021-08-07 08:36:07'),
(18, 'PT. Plasa Telkom Garut', 2, 'Jl. Cikuray No.7, Pakuwon, Kec. Garut Kota', '', '', 'https://www.telkom.co.id/sites', 22, 2, -7.210034992653449, 107.90433457026299, 'icon_1628325389.png', '2021-08-07 08:36:29'),
(19, 'Telekomunikasi Indonesia Tbk. PT - Plasa Telkom Wonosobo', 2, 'Jl. A. Yani No.1, Wonosobo Timur, Wonosobo Tim., Kec. Wonosobo', '', '', '', 23, 1, -7.359593, 109.902982, 'icon_1628325638.png', '2021-08-21 04:05:59'),
(20, 'PT. Puskomedia Indonesia Kreatif', 2, 'Jl. Raya Baturaden No.258, Pabuwaran, Pabuaran, Kec. Purwokerto Utara', '08112617445', '', 'https://www.puskomedia.id', 1, 1, -7.393068, 109.244752, 'icon_1628325687.png', '2021-08-07 08:41:27'),
(21, 'Sekawan Global Komunika', 2, 'Jl. Overste Isdiman No.25, Jatiwinangun, Purwokerto Lor, Kec. Purwokerto Tim', '02817920477', '', 'http://mentarisatria.net.id/', 1, 1, -7.417981492463668, 109.24438687791155, 'icon_1628325763.jpg', '2021-08-07 08:42:43'),
(22, 'PT. Telkom Divisi Regional 1 Sumut', 2, 'JL Prof. DR Jl. Prof. HM. Yamin Sh No.2, Perintis, Kec. Medan Tim', '0614103048', '', '', 20, 7, 3.593104947934326, 98.67757323558233, '', '2021-08-07 07:52:25'),
(23, 'PT. Telkom Witel Makassar', 2, 'Jl. Balaikota No.4, Bulo Gading, Kec. Ujung Pandang', '0411333162', '', 'http://www.telkom.co.id/', 14, 9, -5.13425739872095, 119.40807169325356, '', '2021-08-07 07:55:58'),
(24, 'PT. Telkom Witel Medan', 2, 'Jl. Prof. HM. Yamin Sh No.13, Perintis, Kec. Medan Tim.', '0614530001', '', '', 20, 7, 3.5949724172801703, 98.67945662668448, 'icon_1628518947.png', '2021-08-09 14:22:27'),
(25, 'PT. Tisata Inovasi', 2, 'Gedung Gajah unit ABC, Jl. Dr. Saharjo Jl. Delman Raya No.111, RT.7/RW.11, Kby. Lama Utara, Kec. Tebet', '0218318338', '', '', 24, 4, -6.232136273604156, 106.7723811585092, 'icon_1628325785.png', '2021-08-07 08:43:05'),
(26, 'PT. ULTIMA RASA AKSELERASI', 2, 'Jongkang, Sariharjo, Kec. Ngaglik', '022742883792', 'info@ultraindonesia.com', 'https://ultraindonesia.com/', 21, 5, -7.7449118271748265, 110.37329757791076, 'icon_1628325801.png', '2021-08-07 08:43:21'),
(27, 'PT. Valbury Yogyakarta', 2, 'Jl. Magelang No.75, Kutu Tegal, Sinduadi, Kec. Mlati', '0274623111', '', 'http://www.valburysecurities.co.id/', 21, 5, -7.756384578919374, 110.36216055336901, 'icon_1628325821.jpg', '2021-08-07 08:43:41'),
(28, 'Seven Inc.', 1, 'Jalan Janti Karangjambe, Gg. Arjuna No.59, Jaranan, Banguntapan, Kec. Banguntapan', '02744534571', '', 'https://seven-inc.business.site/', 10, 5, -7.790306686934929, 110.40939580674646, 'icon_1628325837.jpg', '2021-08-07 08:43:57'),
(29, 'SMK Nusantara Lubuk Pakam', 6, 'Jl. Tengku Raja Muda No.1, Petapahan, Kec. Lubuk Pakam', '', '', '', 25, 7, 3.5588065663225885, 98.8686583266845, '', '2021-08-07 08:10:19'),
(30, 'Badan Pusat Statistik Brebes', 7, 'Jl. Letjend MT Haryono No.74, Saditan, Brebes, Kec. Brebes', '0283671168', 'bps3329@bps.go.id', 'https://brebeskab.bps.go.id/', 7, 1, -6.877987504472418, 109.04493342553879, 'icon_1628343322.PNG', '2021-08-07 13:35:22'),
(31, 'Badan Pusat Statistik Kuningan', 7, 'Jl.R.E. Martadinata No. 66 - Cijoho, Kuningan 45513', '0232871662', 'bps3208@bps.go.id', 'https://kuningankab.bps.go.id/', 8, 2, -6.957556477719586, 108.49650191658081, 'icon_1628343341.PNG', '2021-08-07 13:35:41'),
(32, 'Badan Pusat Statistik Pemalang', 7, 'Jl. Tentara Pelajar No.16, Mulyoharjo, Kaligelang, Kec. Pemalang', '0284321169', 'bps3327@bps.go.id', 'https://pemalangkab.bps.go.id/', 9, 1, -6.907612324910302, 109.38872425437408, 'icon_1628343361.PNG', '2021-08-07 13:36:01'),
(33, 'Taman Reptil Adiluhur', 1, 'Jl. Kaleng No. 11 Desa Adiluhur, Dukuh Alasmalang, RT.02/RW.03, Gebang, Adiluhur, Kec. Adimulyo', '08121580021', ' kampungwisatainggriskebumen@gmail.com', 'https://desawisataadiluhur.com/', 11, 1, -7.685479134018603, 109.55176847467607, 'icon_1628343377.jpg', '2021-08-07 13:36:17'),
(34, 'Dinas Kominfo Banyumas', 7, 'Jl. Kolonel Sugiono, Tipar, Purwanegara, Kec. Purwokerto Timur', '0281642679', '', '', 1, 1, -7.427795865741226, 109.23664262861567, 'icon_1628475075.jpg', '2021-08-09 02:11:15'),
(35, 'Dinas Kominfo Cilacap', 7, 'Jalan Sindoro No.36, Sidanegara, Cilacap Tengah, Kandang Macan, Tegalreja, Kec. Cilacap Selatan', '02825563111', 'kominfo@cilacapkab.go.id', 'https://kominfo.cilacapkab.go.id/', 12, 1, -7.721704400374188, 109.00873284088763, 'icon_1628343426.jpg', '2021-08-07 13:37:06'),
(36, 'Dinas Kominfo Mojokerto', 7, 'Jl. Kyai H. Hasyim Ashari No.12, Mergelo, Kauman, Kec. Prajurit Kulon', '0321325470', '', '', 5, 3, -7.461549196948571, 112.43126101020486, 'icon_1628343449.png', '2021-08-07 13:37:29'),
(37, 'Dinas Kominfo Baubau', 7, 'Bukit Wolio Indah, Wolio, Kota Bau-Bau, Sulawesi Tenggara 93713', '0402', 'diskominfo@baubaukota.go.id', 'https://diskominfo.baubaukota.go.id', 13, 10, -5.473439736609646, 122.61453139841066, '', '2021-08-07 13:02:56'),
(38, 'Dinas Kominfo Cirebon', 7, 'Jl. Sunan Drajat No.15, Sumber, Kec. Sumber', '02318330580', '', 'https://www.cirebonkab.go.id/', 15, 2, -6.75927033576079, 108.47597961463309, 'icon_1628343484.jpg', '2021-08-07 13:38:05'),
(39, 'Dinas Kominfo Banten', 7, 'taman k3 ciceri, Jl. Jend. Sudirman No.25, Sumurpecung, Serang', '', '', 'http://diskominfo.serangkota.go.id/', 27, 6, -6.092894947478966, 106.1626994644697, '', '2021-08-07 13:11:16'),
(40, 'Dinas Kominfo DKI Jakarta', 7, 'Jl. Medan Merdeka Selatan No. 8-9 Blok G 3rd Floor, 13th Floor, Blok F 2nd Floor and Blok B 3rd Floor RT.11, RT.11/RW.2, Gambir, Kecamatan Gambir', '0213823355', '', 'https://jakarta.go.id/', 18, 4, -6.181297034080075, 106.82843123902697, 'icon_1628343516.png', '2021-08-07 13:38:36'),
(41, 'Dinas Perhubungan Kota Semarang', 7, 'Jl. Tambak Aji Raya No.5, Tambakaji, Kec. Ngaliyan', '0248662389', '', '', 4, 1, -6.989441967381534, 110.33922846971483, 'icon_1628343533.jpg', '2021-08-07 13:38:53'),
(42, 'LAPAN Sumedang', 7, 'Jl. Raya Bandung-Sumedang No.KM. 31, Haurngombong, Kec. Pamulihan', '', '', '', 16, 2, -6.910296683402302, 107.83694521559143, '', '2021-08-10 00:41:59'),
(43, 'Memeflorist', 1, 'Lamper Lor, Kec. Semarang Selatan', '0857743566434', 'sales@memeflorist.com', 'https://www.memeflorist.com/', 4, 1, -7.004982566200412, 110.43811146787017, 'icon_1628343554.jpg', '2021-08-07 13:39:14'),
(44, 'Baramij Integrasi Teknologi', 1, 'Jalan Suka Asih II No.9 RT/RW:02/07 Kel. Sindangjaya, Kec. Mandalajati ', '08122003016', 'info@baramijintegrasi.com', 'https://www.baramijintegrasi.com', 2, 2, -6.9026535069982256, 107.68520877540959, 'icon_1628343572.png', '2021-08-07 13:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` int(11) NOT NULL,
  `nim` varchar(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `angkatan` int(4) NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` varchar(15) NOT NULL,
  `id_lokasi` int(5) NOT NULL,
  `tgl_mulaiPKL` date NOT NULL,
  `tgl_selesaiPKL` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nim`, `nama`, `jenis_kelamin`, `angkatan`, `alamat`, `no_tlp`, `id_lokasi`, `tgl_mulaiPKL`, `tgl_selesaiPKL`, `created_at`) VALUES
(2, '17102157', 'Mely Sulistiowati', 'P', 2017, '', '', 1, '2020-03-08', '2020-03-09', '2021-08-07 14:26:10'),
(3, '17102155', 'Khurun Ain Muzaki', 'P', 2017, '', '', 15, '0000-00-00', '0000-00-00', '2021-08-07 14:07:56'),
(4, '17102074', 'Alya Cahyaning Tyas', 'P', 2017, '', '', 1, '2020-03-08', '2020-03-09', '2021-08-07 14:04:39'),
(5, '17102169', 'Aina Azalea Ardani', 'P', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:09:32'),
(6, '17102075', 'Annisa Nugraheni', 'P', 2017, '', '', 1, '2020-03-08', '2020-03-09', '2021-08-07 14:03:26'),
(7, '17102188', 'Sang Dara Parameswari', 'P', 2017, '', '', 36, '0000-00-00', '0000-00-00', '2021-08-07 14:09:43'),
(8, '17102051', 'Annisa Eva Ayuning Thiyas', 'P', 2017, '', '', 1, '2020-03-08', '2020-03-09', '2021-08-07 14:06:35'),
(9, '17102187', 'Rizka Fayyadihila', 'P', 2017, '', '', 36, '0000-00-00', '0000-00-00', '2021-08-07 14:10:18'),
(10, '17102185', 'Raden Neomy Lusie Ratna Deseina Budy Putri', 'P', 2017, '', '', 36, '0000-00-00', '0000-00-00', '2021-08-07 14:10:47'),
(11, '17102127', 'Fadlan Sani Mubarok', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:23:18'),
(12, '17102191', 'Wachid Iqbal Maulana', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:24:36'),
(13, '17102172', 'Daffa Rifky', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:25:08'),
(14, '17102122', 'Andi Yulianto', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:25:37'),
(15, '17102021', 'Sarah Nurjihan', 'P', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:26:40'),
(16, '17102160', 'Nizar Tanfidi Asyhari', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:27:06'),
(17, '15102008', 'Ambarweuning Chandra Kalbuadi', 'L', 2015, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:30:56'),
(18, '17102143', 'Vico Meylana Eka Putra', 'L', 2017, '', '', 6, '0000-00-00', '0000-00-00', '2021-08-07 14:28:10'),
(19, '17102115', 'Rindu Hafil Muhammadi', 'L', 2017, '', '', 28, '0000-00-00', '0000-00-00', '2021-08-07 14:30:10'),
(20, '17102110', 'Mochammad Iqbal Fatria', 'L', 2017, '', '', 28, '0000-00-00', '0000-00-00', '2021-08-07 14:30:43'),
(21, '17102103', 'Fadil Al Afgani', 'L', 2017, '', '', 28, '0000-00-00', '0000-00-00', '2021-08-07 14:31:42'),
(22, '17102102', 'Dimas Purwita Sari', 'P', 2017, '', '', 28, '0000-00-00', '0000-00-00', '2021-08-07 14:32:30'),
(23, '17102099', 'Antyka Syahrial', 'P', 2017, '', '', 29, '0000-00-00', '0000-00-00', '2021-08-07 14:33:05'),
(24, '16102191', 'Endhika Emanda Pabella', 'P', 2016, '', '', 27, '0000-00-00', '0000-00-00', '2021-08-07 14:47:46'),
(25, '17102080', 'Faqih abdilah azmi', 'L', 2017, '', '', 26, '0000-00-00', '0000-00-00', '2021-08-07 14:53:38'),
(26, '17102108', 'Lutfi Isti Qomah', 'P', 2017, '', '', 26, '0000-00-00', '0000-00-00', '2021-08-07 14:54:49'),
(27, '17102084', 'Lulu Anggraeni', 'P', 2017, '', '', 26, '0000-00-00', '0000-00-00', '2021-08-07 14:55:17'),
(28, '17102088', 'Mutiara Firdaus', 'P', 2017, '', '', 26, '0000-00-00', '0000-00-00', '2021-08-07 14:56:11'),
(29, '17102093', 'Shakti Kesuma Wibawa', 'L', 2017, '', '', 26, '0000-00-00', '0000-00-00', '2021-08-07 14:57:13'),
(30, '17102010', 'Hikmah Quddustiani', 'P', 2017, '', '', 25, '0000-00-00', '0000-00-00', '2021-08-07 14:57:50'),
(31, '17102116', 'Rut Elisabeth Aprilianti', 'P', 2017, '', '', 24, '0000-00-00', '0000-00-00', '2021-08-07 14:58:49'),
(32, '17102173', 'Diaz Aztisyah', 'P', 2017, '', '', 24, '0000-00-00', '0000-00-00', '2021-08-07 14:59:14'),
(33, '17102101', 'Dewo Rajh Arnada', 'L', 2017, '', '', 23, '0000-00-00', '0000-00-00', '2021-08-07 15:02:28'),
(34, '17102111', 'Muhammad Farhan Razzaq', 'L', 2017, '', '', 23, '0000-00-00', '0000-00-00', '2021-08-07 15:04:47'),
(35, '17102097', 'Agum Eko Pranoto', 'L', 2017, '', '', 23, '0000-00-00', '0000-00-00', '2021-08-07 15:06:09'),
(36, '17102132', 'M. Fadjar Aidil Hasyim Purba', 'L', 2017, '', '', 22, '0000-00-00', '0000-00-00', '2021-08-07 15:07:02'),
(37, '171021038', 'Mhd.Rizal Al Khairi', 'L', 2017, '', '', 22, '0000-00-00', '0000-00-00', '2021-08-07 15:08:20'),
(38, '17102040', 'Muhammad Isra Muntaha Tanjung', 'L', 2017, '', '', 22, '0000-00-00', '0000-00-00', '2021-08-07 15:08:50'),
(39, '17102083', 'Josua Fadli Panggabean', 'L', 2017, '', '', 22, '0000-00-00', '0000-00-00', '2021-08-07 15:09:22'),
(40, '17102086', 'Mirza Sahil', 'L', 2017, '', '', 22, '0000-00-00', '0000-00-00', '2021-08-07 15:10:21'),
(41, '17102034', 'Vandy Ahmad Misry Ar Razy', 'L', 2017, '', '', 21, '0000-00-00', '0000-00-00', '2021-08-07 15:10:47'),
(42, '17102044', 'Rizky Ramadhan', 'L', 2017, '', '', 21, '0000-00-00', '0000-00-00', '2021-08-07 15:11:11'),
(43, '17102025', 'Afriza Ali Zighovit', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:22:21'),
(44, '17102166', 'Theo Felix Harianto Purba', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:22:49'),
(45, '17102184', 'Nugraha Primanda Adi Putra', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:23:22'),
(46, '17102090', 'Reno Agil Saputra', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:23:55'),
(47, '17102171', 'Aris Rafael Tambunan', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:24:18'),
(48, '17102186', 'Reyvaldy Alfida Yanur', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:24:48'),
(49, '17102092', 'Ronald Daniel', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:25:13'),
(50, '17102176', 'Galih Wahyu Nur Syamsudin', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:25:39'),
(51, '17102192', 'Zaky Hanif Testandy', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:25:58'),
(52, '17102098', 'Amanat Dirgantara', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:26:19'),
(53, '17102181', 'Mendi Nuroyan', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:27:00'),
(54, '17102118', 'Taufik Maulidi', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:27:41'),
(55, '17102183', 'Muhammad Hannan Hunafa', 'L', 2017, '', '', 20, '0000-00-00', '0000-00-00', '2021-08-07 15:28:02'),
(56, '16102146', 'Alam patria utama', 'L', 2016, '', '', 19, '0000-00-00', '0000-00-00', '2021-08-07 15:29:01'),
(57, '17102094', 'Tasya Shabrina Salsabillah', 'P', 2017, '', '', 18, '0000-00-00', '0000-00-00', '2021-08-07 15:29:24'),
(58, '17102019', 'Rezi Iwardani Saputri', 'P', 2017, '', '', 5, '0000-00-00', '0000-00-00', '2021-08-07 15:29:57'),
(59, '17102142', 'Thalia Ajeng Purnamasari', 'P', 2017, '', '', 5, '0000-00-00', '0000-00-00', '2021-08-07 15:30:18'),
(60, '17102182', 'Mohtar Khoiruddin', 'L', 2017, '', '', 17, '0000-00-00', '0000-00-00', '2021-08-07 15:30:44'),
(61, '17102156', 'Muhammad Zhafran Dwiki', 'L', 2017, '', '', 17, '0000-00-00', '0000-00-00', '2021-08-07 15:31:10'),
(62, '17102175', 'Fajar Nur Rohman', 'L', 2017, '', '', 17, '0000-00-00', '0000-00-00', '2021-08-07 15:31:39'),
(63, '17102180', 'Ma''ruf Aldi Oktava', 'L', 2017, '', '', 17, '0000-00-00', '0000-00-00', '2021-08-07 15:31:57'),
(64, '17102043', 'Rheni Aprilia Ningrum', 'P', 2017, '', '', 16, '0000-00-00', '0000-00-00', '2021-08-07 15:32:28'),
(65, '17102047', 'Tri Mega Anggraeni', 'P', 2017, '', '', 16, '0000-00-00', '0000-00-00', '2021-08-07 15:32:52'),
(66, '17102049', 'Aghnia NurJumala', 'P', 2017, '', '', 16, '0000-00-00', '0000-00-00', '2021-08-07 15:33:13'),
(67, '17102050', 'Alma Alfiatul Inayah', 'P', 2017, '', '', 16, '0000-00-00', '0000-00-00', '2021-08-07 15:33:34'),
(68, '17102167', 'Vidia Syahputri', 'P', 2017, '', '', 15, '0000-00-00', '0000-00-00', '2021-08-07 15:33:56'),
(69, '17102178', 'Janur Syahputra', 'L', 2017, '', '', 15, '0000-00-00', '0000-00-00', '2021-08-07 15:34:28'),
(70, '17102162', 'Reynaldi Rio Saputro', 'L', 2017, '', '', 15, '0000-00-00', '0000-00-00', '2021-08-07 15:34:46'),
(71, '16102192', 'Faisal Amru', 'L', 2016, '', '', 14, '0000-00-00', '0000-00-00', '2021-08-07 15:35:22'),
(72, '17102035', 'Jinan Ghinia Khansa', 'P', 2017, '', '', 13, '0000-00-00', '0000-00-00', '2021-08-07 15:36:00'),
(73, '17102052', 'Bahri', 'L', 2017, '', '', 13, '0000-00-00', '0000-00-00', '2021-08-07 15:36:22'),
(74, '17102076', 'Bayu Daffa Mujo Yulianto', 'L', 2017, '', '', 12, '0000-00-00', '0000-00-00', '2021-08-07 15:36:52'),
(75, '17102024', 'Wahyu Indra Yuda', 'L', 2017, '', '', 12, '0000-00-00', '0000-00-00', '2021-08-07 15:37:12'),
(76, '17102041', 'Nursyifa Aprillia Putri', 'P', 2017, '', '', 12, '0000-00-00', '0000-00-00', '2021-08-07 15:37:47'),
(77, '17102045', 'Sean Reza Arienola', 'L', 2017, '', '', 12, '0000-00-00', '0000-00-00', '2021-08-07 15:38:05'),
(78, '17102189', 'Surya Adi Widiarto', 'L', 2017, '', '', 11, '0000-00-00', '0000-00-00', '2021-08-07 15:38:25'),
(79, '17102190', 'Thowaf Fuad Hasan', 'L', 2017, '', '', 11, '0000-00-00', '0000-00-00', '2021-08-07 15:38:56'),
(80, '17102095', 'Ulung Priyo Bintoro', 'L', 2017, '', '', 11, '0000-00-00', '0000-00-00', '2021-08-07 15:39:19'),
(81, '17102158', 'Mohammad Aditya Rifki Firmansyah', 'L', 2017, '', '', 11, '0000-00-00', '0000-00-00', '2021-08-07 15:39:48'),
(82, '16102016', 'Egi Prananta Ginting', 'L', 2016, '', '', 10, '0000-00-00', '0000-00-00', '2021-08-07 15:40:18'),
(83, '17102065', 'Panggiarta Rindah Pratama', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:40:53'),
(84, '17102007', 'Elen Ardiyanto', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:42:15'),
(85, '17102033', 'Habib Ali Habsyi', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:42:38'),
(86, '17102068', 'Rizqi Maulana Bahtiar', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:42:59'),
(87, '17102020', 'Rizky Aulia Fansyuri', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:43:26'),
(88, '17102046', 'Syahliana Iqbal Nurhidayat', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:43:47'),
(89, '17102072', 'Wiwit Farianto', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:44:12'),
(90, '17102026', 'Akhmad Triaji', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:44:34'),
(91, '17102056', 'Fakhri Azmi Husaini', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:44:54'),
(92, '17102030', 'Didik Priyoga', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:45:12'),
(93, '17102064', 'Muhammad Ivan Aprilian', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:45:33'),
(94, '17102003', 'Angga Hernanto', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:45:51'),
(95, '17102032', 'Fajrin Septiana Nugraha', 'L', 2017, '', '', 7, '0000-00-00', '0000-00-00', '2021-08-07 15:46:09'),
(96, '17102165', 'Sultan Rafi Raihan', 'L', 2017, '', '', 9, '0000-00-00', '0000-00-00', '2021-08-07 15:46:59'),
(97, '17102177', 'Haski Pratomo', 'L', 2017, '', '', 9, '0000-00-00', '0000-00-00', '2021-08-07 15:47:19'),
(98, '17102135', 'Muhammad Farhansyah', 'L', 2017, '', '', 43, '0000-00-00', '0000-00-00', '2021-08-07 15:49:03'),
(99, '17102123', 'Arif Agustyawan', 'L', 2017, '', '', 43, '0000-00-00', '0000-00-00', '2021-08-07 15:49:26'),
(100, '17102151', 'Fahmi moearif', 'L', 2017, '', '', 42, '0000-00-00', '0000-00-00', '2021-08-07 15:50:02'),
(101, '17102139', 'Rio Chaerudin', 'L', 2017, '', '', 41, '0000-00-00', '0000-00-00', '2021-08-07 15:50:45'),
(102, '17102153', 'Hary Indra Permana', 'L', 2017, '', '', 41, '0000-00-00', '0000-00-00', '2021-08-07 15:51:08'),
(103, '17102139', 'Reynaldo Berluskoni Girsang', 'L', 2017, '', '', 41, '0000-00-00', '0000-00-00', '2021-08-07 15:51:32'),
(104, '17102001', 'Adam Ikbal Perdana', 'L', 2017, '', '', 40, '0000-00-00', '0000-00-00', '2021-08-07 15:52:07'),
(105, '17102066', 'Rendi Priambodo', 'L', 2017, '', '', 40, '0000-00-00', '0000-00-00', '2021-08-07 15:52:36'),
(106, '17102144', 'Yudha Trisnahadi', 'L', 2017, '', '', 40, '0000-00-00', '0000-00-00', '2021-08-07 15:52:59'),
(107, '17102117', 'Sheren Afryan Tiastama', 'P', 2017, '', '', 39, '0000-00-00', '0000-00-00', '2021-08-07 15:53:26'),
(108, '17102077', 'Desi Natalia BR. Sidauruk', 'P', 2017, '', '', 38, '0000-00-00', '0000-00-00', '2021-08-07 15:53:48'),
(109, '17102159', 'Muhammad Fathan Nurrohman', 'L', 2017, '', '', 38, '0000-00-00', '0000-00-00', '2021-08-07 15:54:10'),
(110, '17102170', 'Andya Lesmana', 'L', 2017, '', '', 38, '0000-00-00', '0000-00-00', '2021-08-07 15:54:31'),
(111, '17102054', 'Dilla Febria Elisty', 'P', 2017, '', '', 38, '0000-00-00', '0000-00-00', '2021-08-07 15:54:55'),
(112, '17102179', 'La Ode Hasrul', 'L', 2017, '', '', 37, '0000-00-00', '0000-00-00', '2021-08-07 15:55:15'),
(113, '17102114', 'Rey Anggara', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:55:54'),
(114, '17102012', 'Lingga Jamaluddin', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:56:18'),
(115, '17102014', 'Meydika Candra Aji', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:56:39'),
(116, '17102087', 'Muhammad Faqih Dwi Saputra', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:57:07'),
(117, '17102089', 'Parwita Putri Hardianto', 'P', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:57:31'),
(118, '17102002', 'Akbar Borneo Dammara', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:57:54'),
(119, '17102008', 'Fajar Sulistiono', 'L', 2017, '', '', 35, '0000-00-00', '0000-00-00', '2021-08-07 15:58:13'),
(120, '16102025', 'Nadya Rahma Lisanti', 'P', 2016, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:00:48'),
(121, '17102129', 'Haris Pradana', 'L', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 15:58:56'),
(122, '17102017', 'Nur Khanifah', 'P', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 15:59:21'),
(123, '16102026', 'Nurfika Farahdi Amalia', 'P', 2016, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:00:10'),
(124, '17102136', 'Ngumron Makmum', 'L', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:00:26'),
(125, '17102036', 'Lisi Refsiliana', 'P', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:01:08'),
(126, '17102119', 'Umar Rella Adhitya', 'L', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:01:35'),
(127, '17102150', 'Diva Aditya Pratama', 'L', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:02:01'),
(128, '17102138', 'Reyfaldi Samuel Agustavo', 'L', 2017, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:02:24'),
(129, '16102010', 'Desta Cesariandy Dwi Pangestu', 'P', 2016, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:03:11'),
(130, '16102188', 'Bunga Permata Sari', 'P', 2016, '', '', 34, '0000-00-00', '0000-00-00', '2021-08-07 16:03:42'),
(131, '16102037', 'Abet Nego Sembiring', 'L', 2016, '', '', 33, '0000-00-00', '0000-00-00', '2021-08-07 16:06:02'),
(132, '17102028', 'Bagus Satria Pratama', 'L', 2017, '', '', 8, '0000-00-00', '0000-00-00', '2021-08-07 16:06:29'),
(133, '16102001', 'Abdul Adzib Budihartono Nugroho', 'L', 2016, '', '', 8, '0000-00-00', '0000-00-00', '2021-08-07 16:06:51'),
(134, '16102023', 'Muhammad Fajariyandi Saputra', 'L', 2016, '', '', 44, '0000-00-00', '0000-00-00', '2021-08-07 16:07:55'),
(135, '17102100', 'Cahyani Ainun Awaliyah', 'P', 2017, '', '', 4, '0000-00-00', '0000-00-00', '2021-08-07 16:07:39'),
(136, '17102106', 'Ilham Fadhilah Akbar', 'L', 2017, '', '', 4, '0000-00-00', '0000-00-00', '2021-08-07 16:08:19'),
(137, '17102141', 'Sintiya', 'P', 2017, '', '', 32, '0000-00-00', '0000-00-00', '2021-08-07 16:08:50'),
(138, '17102037', 'Marcel Agustine Bacsafra', 'P', 2017, '', '', 31, '0000-00-00', '0000-00-00', '2021-08-07 16:09:15'),
(139, '17102182', 'Ika Rahmawati Suci', 'P', 2017, '', '', 30, '0000-00-00', '0000-00-00', '2021-08-07 16:09:43'),
(140, '17102091', 'Rifky Syahreza Fahlevi Suwartono', 'L', 2017, '', '', 30, '0000-00-00', '0000-00-00', '2021-08-07 16:10:02'),
(141, '17102130', 'Isnaini Rizki Atika', 'P', 2017, '', '', 3, '0000-00-00', '0000-00-00', '2021-08-07 16:10:26'),
(142, '17102131', 'Khofifah Putriyani', 'P', 2017, '', '', 3, '0000-00-00', '0000-00-00', '2021-08-07 16:10:46'),
(143, '17102149', 'Dian Finaliamartha', 'P', 2017, '', '', 3, '0000-00-00', '0000-00-00', '2021-08-07 16:11:04'),
(144, '17102154', 'Isnan Yunus Alhalim', 'L', 2017, '', '', 3, '0000-00-00', '0000-00-00', '2021-08-07 16:11:25'),
(145, '17102128', 'Felia Citra Dwiyani Putri Rosyadi', 'L', 2017, '', '', 3, '0000-00-00', '0000-00-00', '2021-08-07 16:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id_options` int(11) NOT NULL,
  `option_name` varchar(500) NOT NULL,
  `option_value` longtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id_options`, `option_name`, `option_value`, `timestamp`) VALUES
(1, 'Tentang PKL', 'Praktik  Kerja  Lapangan/Kerja  Praktik  (PKL/KP)  merupakan implementasi  dari  pengetahuan  dan  keterampilan  yang  telah  didapatkan selama  masa kuliah untuk berkontribusi  membantu pemecahan  masalah di berbagai  perusahaan  atau  instansi  yang  terkait  dengan  bidang   yang  telah dipelajari pada masing - masing program studinya.', '2021-08-09 14:32:33'),
(2, 'Sistem Informasi Geografis', 'Lokasi PKL Mahasiswa Informatika', '2021-08-02 13:16:21'),
(5, 'Caption', 'Temukan Instansi Favoritmu Disini!', '2021-08-02 13:16:38'),
(6, 'Footer', 'Sistem Informasi Geografis Lokasi PKL merupakan website yang berisi informasi mengenai pemetaan lokasi PKL beserta data lain yang berkaitan dengan kegiatan PKL mahasiswa S1 Teknik Informatika, Institut Teknologi Telkom Purwokerto.', '2021-08-03 07:34:01'),
(7, 'nama web', 'SIG Lokasi PKL', '2021-08-09 14:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE `propinsi` (
  `id_propinsi` int(11) NOT NULL,
  `nama_prop` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`id_propinsi`, `nama_prop`) VALUES
(1, 'Jawa Tengah'),
(2, 'Jawa Barat'),
(3, 'Jawa Timur'),
(4, 'DKI Jakarta'),
(5, 'DI Yogyakarta'),
(6, 'Banten'),
(7, 'Sumatera Utara'),
(8, 'Kalimantan Selatan'),
(9, 'Sulawesi Selatan'),
(10, 'Sulawesi Tenggara');

-- --------------------------------------------------------

--
-- Stand-in structure for view `top_10`
--
CREATE TABLE `top_10` (
`nama_instansi` varchar(500)
,`jenis` varchar(50)
,`alamat` text
,`website` varchar(500)
,`icon` varchar(500)
,`nama_kab` varchar(50)
,`nama_prop` varchar(50)
,`jml_mhs` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `foto` varchar(500) DEFAULT NULL,
  `level` tinyint(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `phone`, `foto`, `level`, `created_at`) VALUES
(2, 'admin', 'admin', 'Admin 1', '0123456789', 'foto_1628432516.png', 1, '2021-08-23 04:58:38'),
(3, 'mimin', '123', 'Admin 2', '0898765432', 'foto_1628260978.png', 2, '2021-08-09 13:27:56');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_1`
--
CREATE TABLE `view_1` (
`nama_instansi` varchar(500)
,`jenis` varchar(50)
,`alamat` text
,`website` varchar(500)
,`icon` varchar(500)
,`nama_kab` varchar(50)
,`nama_prop` varchar(50)
,`jml_mhs` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_lokasi`
--
CREATE TABLE `view_lokasi` (
`nama_instansi` varchar(500)
,`jenis` varchar(50)
,`alamat` text
,`website` varchar(500)
,`icon` varchar(500)
,`nama_kab` varchar(50)
,`nama_prop` varchar(50)
,`nama` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `top_10`
--
DROP TABLE IF EXISTS `top_10`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `top_10`  AS  select `view_1`.`nama_instansi` AS `nama_instansi`,`view_1`.`jenis` AS `jenis`,`view_1`.`alamat` AS `alamat`,`view_1`.`website` AS `website`,`view_1`.`icon` AS `icon`,`view_1`.`nama_kab` AS `nama_kab`,`view_1`.`nama_prop` AS `nama_prop`,`view_1`.`jml_mhs` AS `jml_mhs` from `view_1` order by `view_1`.`jml_mhs` desc limit 10 ;

-- --------------------------------------------------------

--
-- Structure for view `view_1`
--
DROP TABLE IF EXISTS `view_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_1`  AS  select `view_lokasi`.`nama_instansi` AS `nama_instansi`,`view_lokasi`.`jenis` AS `jenis`,`view_lokasi`.`alamat` AS `alamat`,`view_lokasi`.`website` AS `website`,`view_lokasi`.`icon` AS `icon`,`view_lokasi`.`nama_kab` AS `nama_kab`,`view_lokasi`.`nama_prop` AS `nama_prop`,count(`view_lokasi`.`nama`) AS `jml_mhs` from `view_lokasi` group by `view_lokasi`.`nama_instansi` ;

-- --------------------------------------------------------

--
-- Structure for view `view_lokasi`
--
DROP TABLE IF EXISTS `view_lokasi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lokasi`  AS  select `a`.`nama_instansi` AS `nama_instansi`,`b`.`jenis` AS `jenis`,`a`.`alamat` AS `alamat`,`a`.`website` AS `website`,`a`.`icon` AS `icon`,`c`.`nama_kab` AS `nama_kab`,`d`.`nama_prop` AS `nama_prop`,`e`.`nama` AS `nama` from ((((`lokasi` `a` join `jenis_lokasi` `b`) join `kabupaten` `c`) join `propinsi` `d`) join `mahasiswa` `e`) where ((`a`.`id_jenis` = `b`.`id_jenis`) and (`a`.`id_kabupaten` = `c`.`id_kabupaten`) and (`a`.`id_propinsi` = `d`.`id_propinsi`) and (`a`.`id_lokasi` = `e`.`id_lokasi`)) order by `a`.`id_lokasi` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenis_lokasi`
--
ALTER TABLE `jenis_lokasi`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id_kabupaten`);

--
-- Indexes for table `koordinat`
--
ALTER TABLE `koordinat`
  ADD PRIMARY KEY (`id_koordinat`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id_link`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id_options`);

--
-- Indexes for table `propinsi`
--
ALTER TABLE `propinsi`
  ADD PRIMARY KEY (`id_propinsi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenis_lokasi`
--
ALTER TABLE `jenis_lokasi`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `koordinat`
--
ALTER TABLE `koordinat`
  MODIFY `id_koordinat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id_link` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id_options` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `propinsi`
--
ALTER TABLE `propinsi`
  MODIFY `id_propinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
